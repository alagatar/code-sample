#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, HapticFeedbackType) {
    SelectionChanged = 0,
	ImpactLight = 1,
	ImpactMedium = 2,
	ImpactHeavy = 3,
	NotificationSuccess = 4,
	NotificationWarning = 5,
	NotificationError = 6
};

@interface iOSHapticFeedback:NSObject
{
    UISelectionFeedbackGenerator *_selectionFeedback;
    UIImpactFeedbackGenerator *_lightImpactFeedback;
    UIImpactFeedbackGenerator *_mediumImpactFeedback;
    UIImpactFeedbackGenerator *_heavyImpactFeedback;
    UINotificationFeedbackGenerator *_notificationFeedback;
}

- (void)prepareSelectionChanged;
- (void)prepareImpactLight;
- (void)prepareImpactMedium;
- (void)prepareImpactHeavy;
- (void)prepareNotification;

- (void)triggerSelectionChanged;
- (void)triggerImpactLight;
- (void)triggerImpactMedium;
- (void)triggerImpactHeavy;
- (void)triggerNotificationSuccess;
- (void)triggerNotificationWarning;
- (void)triggerNotificationError;

@end