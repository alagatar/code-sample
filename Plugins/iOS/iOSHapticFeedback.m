#import "iOSHapticFeedback.h"
#import <UIKit/UIKit.h>

@implementation iOSHapticFeedback

- (void)prepareSelectionChanged
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    //NSLog(@"Prepare SelectionChanged");

    if(_selectionFeedback == nil)
    {
        _selectionFeedback = [[UISelectionFeedbackGenerator alloc] init];
    }
    [_selectionFeedback prepare];
}

- (void)prepareImpactLight
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Prepare ImpactLight");

    if(_lightImpactFeedback == nil)
    {
        _lightImpactFeedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
    }
    [_lightImpactFeedback prepare];
}

- (void)prepareImpactMedium
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    //NSLog(@"Prepare ImpactMedium");

    if(_mediumImpactFeedback == nil)
    {
        _mediumImpactFeedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleMedium];
    }
    [_mediumImpactFeedback prepare];
}

- (void)prepareImpactHeavy
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    //NSLog(@"Prepare ImpactHeavy");

    if(_heavyImpactFeedback == nil)
    {
        _heavyImpactFeedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleHeavy];
    }
    [_heavyImpactFeedback prepare];
}

- (void)prepareNotification
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Prepare Notification");

    if(_notificationFeedback == nil)
    {
        _notificationFeedback = [[UINotificationFeedbackGenerator alloc] init];
    }
    [_notificationFeedback prepare];
}

- (void)triggerSelectionChanged
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger SelectionChanged");

    if(_selectionFeedback == nil)
    {
        _selectionFeedback = [[UISelectionFeedbackGenerator alloc] init];
    }
    [_selectionFeedback selectionChanged];
}

- (void)triggerImpactLight
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger ImpactLight");

    if(_lightImpactFeedback == nil)
    {
        _lightImpactFeedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
    }
    [_lightImpactFeedback impactOccurred];
}

- (void)triggerImpactMedium
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger ImpactMedium");

    if(_mediumImpactFeedback == nil)
    {
        _mediumImpactFeedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleMedium];
    }
    [_mediumImpactFeedback impactOccurred];
}

- (void)triggerImpactHeavy
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger ImpactHeavy");

    if(_heavyImpactFeedback == nil)
    {
        _heavyImpactFeedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleHeavy];
    }
    [_heavyImpactFeedback impactOccurred];
}

- (void)triggerNotificationSuccess
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger NotificationSuccess");

    if(_notificationFeedback == nil)
    {
        _notificationFeedback = [[UINotificationFeedbackGenerator alloc] init];
    }
    [_notificationFeedback notificationOccurred:UINotificationFeedbackTypeSuccess];
}

- (void)triggerNotificationWarning
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger NotificationWarning");

    if(_notificationFeedback == nil)
    {
        _notificationFeedback = [[UINotificationFeedbackGenerator alloc] init];
    }
    [_notificationFeedback notificationOccurred:UINotificationFeedbackTypeWarning];
}

- (void)triggerNotificationError
{
    if(![UIFeedbackGenerator class])
    {
        return;
    }
    // NSLog(@"Trigger NotificationError");

    if(_notificationFeedback == nil)
    {
        _notificationFeedback = [[UINotificationFeedbackGenerator alloc] init];
    }
    [_notificationFeedback notificationOccurred:UINotificationFeedbackTypeError];
}

# pragma mark - C API

static iOSHapticFeedback* instance = NULL;

void _trigger(HapticFeedbackType feedbackType)
{
    if(instance == NULL)
    {
        instance = [[iOSHapticFeedback alloc] init];
    }

    switch(feedbackType)
    {
        case SelectionChanged:
            [instance triggerSelectionChanged];
        break;

        case ImpactLight:
            [instance triggerImpactLight];
        break;

        case ImpactMedium:
            [instance triggerImpactMedium];
        break;

        case ImpactHeavy:
            [instance triggerImpactHeavy];
        break;

        case NotificationSuccess:
            [instance triggerNotificationSuccess];
        break;

        case NotificationWarning:
            [instance triggerNotificationWarning];
        break;

        case NotificationError:
            [instance triggerNotificationError];
        break;

        default:
        break;
    }
}

void _prepare(HapticFeedbackType feedbackType)
{
    if(instance == NULL)
    {
        instance = [[iOSHapticFeedback alloc] init];
    }

    switch(feedbackType)
    {
        case SelectionChanged:
            [instance prepareSelectionChanged];
        break;

        case ImpactLight:
            [instance prepareImpactLight];
        break;

        case ImpactMedium:
            [instance prepareImpactMedium];
        break;

        case ImpactHeavy:
            [instance prepareImpactHeavy];
        break;

        case NotificationSuccess:
        case NotificationWarning:
        case NotificationError:
            [instance prepareNotification];
        break;

        default:
        break;
    }
}

@end