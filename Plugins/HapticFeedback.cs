using UnityEngine;
using System.Runtime.InteropServices;

public enum HapticFeedbackType
{
	SelectionChanged = 0,
	ImpactLight,
	ImpactMedium,
	ImpactHeavy,
	NotificationSuccess,
	NotificationWarning,
	NotificationError
}

public class HapticFeedback:MonoBehaviour
{
#if UNITY_IOS
    [DllImport("__Internal")]
    extern private static void _prepare(HapticFeedbackType feedbackType);
    [DllImport("__Internal")]
    extern private static void _trigger(HapticFeedbackType feedbackType);
#endif

    public static void prepare(HapticFeedbackType feedbackType)
    {
        #if UNITY_IOS
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _prepare(feedbackType);
        }
        #endif
    }

    public static void trigger(HapticFeedbackType feedbackType)
    {
        #if UNITY_IOS
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _trigger(feedbackType);
        }
        #endif
    }
}