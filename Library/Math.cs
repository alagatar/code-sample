﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivat
{
    public class Math
    {
        private const int MAX_DECIMAL = 5;

        public static float calcDistOfDots(float x1, float y1, float x2, float y2)
        {
            return Mathf.Sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        }

        public static FloatPoint getMiddlePoint(float x1, float y1, float x2, float y2)
        {
            float difX = (x1 - x2) / 2.0f;
            float difY = (y1 - y2) / 2.0f;

            FloatPoint middlePosition = new FloatPoint();
            middlePosition.x = x2 + difX;
            middlePosition.y = y2 + difY;
            return middlePosition;
        }

        public static IntPoint calcChanceOptions(float chance, bool debug = false)
        {
            IntPoint options = new IntPoint();

            if(chance == 0)
            {
                return options;
            }

            float decimalPlaces = calcDecimal(chance);
            if(decimalPlaces > MAX_DECIMAL)
            {
                decimalPlaces = MAX_DECIMAL;
                chance = (float)System.Math.Round(chance, MAX_DECIMAL);
            }

            int total = (int)System.Math.Pow(10d, decimalPlaces);
            int count = (int)System.Math.Round(chance * total);

            if(debug)
            {
                Debug.Log(string.Format("Chance:{0} = {1}/{2}", chance, count, total));
            }
            
            options.x = total;
            options.y = count;

            return options;
        }

        public static float calcDecimal(float value)
        {
            string strValue = value.ToString("R");
            if(strValue.IndexOf(".") == -1)
            {
                return 0;
            }
            return strValue.Split('.')[1].Length;
        }

        public static void shuffleList<T>(List<T> arr)
        {
            if(arr == null)
            {
                return;
            }

            int count = arr.Count;
            for(int i = 0; i < count; i++)
            {
                int j = Random.Range(0, count - 1);
                var a = arr[i];
                var b = arr[j];
                arr[i] = b;
                arr[j] = a;
            }
        }

        public static float roundDec(float value, int places)
        {
            float powValue = Mathf.Pow(10, places);
            value *= powValue;
            return Mathf.Round(value) / powValue;
        }

        public static Vector2 randomPointInCircle(Vector2 position, float radius)
        {
            return position + (Random.insideUnitCircle * radius);
        }

        public static Vector2 randomPointInBox(Vector2 position, Vector2 size)
        {
            Vector2 result = new Vector2();
            float halfW = size.x / 2f;
            float halfH = size.y / 2f;
            result.x = Random.Range(position.x - halfW, position.x + halfW);
            result.y = Random.Range(position.y - halfH, position.y + halfH);
            return result;
        }
    }
}