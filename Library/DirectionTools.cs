﻿namespace Vivat
{
    public class DirectionTools
    {
        public static Direction rotateDirection(Direction dir)
        {
            switch(dir)
            {
                case Direction.Up:
                    return Direction.Right;

                case Direction.Right:
                    return Direction.Down;

                case Direction.Down:
                    return Direction.Left;

                case Direction.Left:
                    return Direction.Up;
            }
            return Direction.None;
        }

        public static Direction invertDirection(Direction dir)
        {
            dir = invertDirectionHorizontal(dir);
            dir = invertDirectionVertical(dir);
            return dir;
        }

        public static Direction invertDirectionHorizontal(Direction dir)
        {
            if(dir == Direction.Left)
            {
                dir = Direction.Right;
            }
            else if(dir == Direction.Right)
            {
                dir = Direction.Left;
            }
            return dir;
        }

        public static Direction invertDirectionVertical(Direction dir)
        {
            if(dir == Direction.Up)
            {
                dir = Direction.Down;
            }
            else if(dir == Direction.Down)
            {
                dir = Direction.Up;
            }
            return dir;
        }

        public static IntPoint getChangeByDirection(Direction dir)
        {
            IntPoint point = new IntPoint();
            point.x = 0;
            point.y = 0;

            switch(dir)
            {
                case Direction.Up:
                    point.y++;
                    break;

                case Direction.Right:
                    point.x++;
                    break;

                case Direction.Down:
                    point.y--;
                    break;

                case Direction.Left:
                    point.x--;
                    break;
            }

            return point;
        }
    }
}
