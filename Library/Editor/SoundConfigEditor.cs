﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Vivat;

[CustomEditor(typeof(SoundsConfig))]
public class SoundConfigEditor:Editor
{
}

[CustomPropertyDrawer(typeof(SoundConfig))]
public class SoundConfigDrawer:PropertyDrawer
{
    Dictionary<string, AudioSource> previewSounds;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        int origIndent = EditorGUI.indentLevel;
        drawSoundProperty(position, property, label);
        EditorGUI.indentLevel = origIndent;
    }

    protected static bool showItems = true;
    void drawSoundProperty(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty clipProp = property.FindPropertyRelative("clip");
        SerializedProperty nameProp = property.FindPropertyRelative("name");
        SerializedProperty volumeProp = property.FindPropertyRelative("volume");

        property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, label.text, true);
        if(property.isExpanded)
        {
            EditorGUILayout.PropertyField(clipProp);
            EditorGUILayout.PropertyField(nameProp);
            EditorGUILayout.PropertyField(volumeProp);

            GUILayout.BeginHorizontal();
            GUILayout.Space(16);

            float volume = volumeProp.floatValue;
            string name = nameProp.stringValue;

            if(GUILayout.Button("Play"))
            {
                AudioClip clip = (AudioClip)clipProp.objectReferenceValue;
                playSound(name, clip, volume);
            }

            if(GUILayout.Button("Stop"))
            {
                stopSound(name);
            }

            upVolume(name, volume);

            GUILayout.EndHorizontal();
        }        
    }

    void upVolume(string id, float volume)
    {
        AudioSource source = getSourceByName(id);
        if (source != null && 
            source.isPlaying && 
            source.volume != volume)
        {
            source.volume = volume;
        }
    }

    void playSound(string id, AudioClip clip, float volume)
    {
        stopSound(id);

        AudioSource source = getSourceByName(id);
        source.volume = volume;
        source.clip = clip;
        source.loop = false;
        source.Play();
    }

    void stopSound(string id)
    {
        AudioSource source = getSourceByName(id);
        if(source != null && 
            source.isPlaying)
        {
            source.Stop();
        }
    }

    AudioSource getSourceByName(string id)
    {
        if(previewSounds == null)
        {
            previewSounds = new Dictionary<string, AudioSource>();
        }

        if(!previewSounds.ContainsKey(id))
        {
            GameObject soundObject = new GameObject();
            soundObject.hideFlags = HideFlags.HideAndDontSave;

            AudioSource soundSource = soundObject.AddComponent<AudioSource>();
            soundSource.playOnAwake = false;

            previewSounds.Add(id, soundSource);           
        }

        return previewSounds[id];
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 0f;
    }
}