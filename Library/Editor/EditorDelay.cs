﻿using UnityEditor;
using UnityEngine.Events;

namespace Vivat
{
    public class EditorDelay
    {
        bool inited = false;
        int timeout;
        UnityAction<object[]> callback;
        object[] args;

        public static EditorDelay delay(int newTimeout, UnityAction<object[]> newCallback = null, object[] newArgs = null)
        {
            EditorDelay link = new EditorDelay();
            link.start(newTimeout, newCallback, newArgs);
            return link;
        }

        public void start(int newTimeout, UnityAction<object[]> newCallback = null, object[] newArgs = null)
        {
            timeout = newTimeout;
            callback = newCallback;
            args = newArgs;

            if(!inited)
            {
                EditorApplication.update += update;
            }
        }

        void update()
        {
            timeout--;
            if(timeout == 0)
            {
                complete();
            }
        }

        void complete()
        {
            inited = false;
            EditorApplication.update -= update;

            if(callback != null)
            {
                callback(args);
            }
        }
    }
}