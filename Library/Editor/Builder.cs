﻿using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System;

namespace Vivat
{
    public class Builder:Editor
    {
        static int connectDelay = 5;
        static int uploadDelay = 40;

        [MenuItem("Tools/Android builder/Victor/Init")]
        public static void InitVictor()
        {
            initToDevice("27c238ed");
        }

        static void initToDevice(string deviceId)
        {
            Process proc = new Process();

            if(Application.platform == RuntimePlatform.OSXEditor)
            {
                proc.StartInfo.FileName = "/usr/local/bin/adb";
            }
            else
            {
                proc.StartInfo.FileName = "adb.exe";
            }

            proc.StartInfo.Arguments = "-s " + deviceId + " tcpip 5555";
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;
            
            proc.Start();
        }

        [MenuItem("Tools/Android builder/Victor/Upload")]
        public static void UploadVictor()
        {
            connectToDevice("192.168.1.5");
            EditorDelay.delay(uploadDelay, uploadLastBuild);
        }

        static void connectToDevice(string deviceIp)
        {
            disconnect();

            var args = new object[] { deviceIp };
            EditorDelay.delay(connectDelay, runConnectToDevice, args);
        }

        static void runConnectToDevice(object[] args)
        {
            string deviceIp = (string)args[0];

            Process proc = new Process();
            if(Application.platform == RuntimePlatform.OSXEditor)
            {
                proc.StartInfo.FileName = "/usr/local/bin/adb";
            }
            else
            {
                proc.StartInfo.FileName = "adb.exe";
            }

            proc.StartInfo.Arguments = "connect " + deviceIp;

            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;

            proc.Start();
        }

        static void disconnect()
        {
            Process proc = new Process();
            if(Application.platform == RuntimePlatform.OSXEditor)
            {
                proc.StartInfo.FileName = "/usr/local/bin/adb";
            }
            else
            {
                proc.StartInfo.FileName = "adb.exe";
            }
            proc.StartInfo.Arguments = "disconnect";

            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;

            proc.Start();
        }

        static void uploadLastBuild(object[] args)
        {
            int buildId = getLastBuildNumber();

            string currentDate = DateTime.Now.ToString("MM.dd.yyyy HH:mm:ss");
            UnityEngine.Debug.Log("Upload build #" + buildId + "   Time:" + currentDate);

            EditorDelay.delay(1, runUploadBuild);
        }

        static void runUploadBuild(object[] args)
        {
            int buildId = getLastBuildNumber();
            string assetPath = Application.dataPath;
            string rootPath = assetPath.Substring(0, assetPath.Length - 7);
            string path = rootPath + "/Builds/Android/game_" + buildId + ".apk";

            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            if(Application.platform == RuntimePlatform.OSXEditor)
            {
                process.StartInfo.FileName = "/usr/local/bin/adb";
            }
            else
            {
                process.StartInfo.FileName = "adb.exe";
            }
            process.StartInfo.Arguments = "install -r '" + path + "'";;
            
            process.EnableRaisingEvents = true;
            process.Exited += new EventHandler(onCompleteUpload);
            process.Start();
        }

        static void onCompleteUpload(object sender, EventArgs e)
        {
            (sender as Process).Exited -= onCompleteUpload;

            UnityEngine.Debug.Log("Complete uploading. Exit code:" + (sender as Process).ExitCode);
        }

        [MenuItem("Tools/Android builder/Dima/Init")]
        public static void InitDima()
        {
            initToDevice("57d05575");
        }

        [MenuItem("Tools/Android builder/Dima/Upload")]
        public static void UploadDima()
        {
            connectToDevice("192.168.1.3");
            EditorDelay.delay(uploadDelay, uploadLastBuild);
        }

        [MenuItem("Tools/Android builder/Restart")]
        public static void Restart()
        {
            Process proc = new Process();
            if(Application.platform == RuntimePlatform.OSXEditor)
            {
                proc.StartInfo.FileName = "/usr/local/bin/adb";
            }
            else
            {
                proc.StartInfo.FileName = "adb.exe";
            }
            proc.StartInfo.Arguments = "kill-server";

            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.UseShellExecute = false;

            proc.Start();
        }

        [MenuItem("Tools/Android builder/Build")]
        static void buildAndroidNormal()
        {
            buildAndroid();
        }

        [MenuItem("Tools/Android builder/Build debug")]
        static void buildDebugAndroid()
        {
            buildAndroid(true);
        }

        static void buildAndroid(bool debug = false)
        {
            int buildNumber = getLastBuildNumber();
            buildNumber++;
            string currentDate = DateTime.Now.ToString("MM.dd.yyyy HH:mm:ss");
            UnityEngine.Debug.Log("Building build #" + buildNumber + "   Time:" + currentDate);

            var args = new object[] { buildNumber, debug };
            EditorDelay.delay(3, runBuildAndroid, args);
        }

        static void runBuildAndroid(object[] args)
        {
            var scenes = getActiveScenes();
            int buildId = (int)args[0];
            bool debug = (bool)args[1];
            saveBuildNumber(buildId);
            var path = string.Format("Builds/Android/game_{0}.apk", buildId);

            BuildOptions options;
            if(debug)
            {
                options = BuildOptions.Development | BuildOptions.AllowDebugging;
            }
            else
            {
                options = BuildOptions.None;
            }
            BuildPipeline.BuildPlayer(scenes, path, BuildTarget.Android, options);
        }

        static string[] getActiveScenes()
        {
            var scenes = new List<string>();
            foreach(var scene in EditorBuildSettings.scenes)
            {
                if(scene != null && scene.enabled)
                {
                    scenes.Add(scene.path);
                }
            }
            return scenes.ToArray();
        }

        static int getLastBuildNumber()
        {
            if(!Directory.Exists("Builds/Android"))
            {
                Directory.CreateDirectory("Builds/Android");
            }

            if(!File.Exists("Builds/Android/.build"))
            {
                File.WriteAllText("Builds/Android/.build", "0");
            }

            string text = File.ReadAllText("Builds/Android/.build");
            int buildNumber;
            if(!int.TryParse(text, out buildNumber))
            {
                buildNumber = 0;
            }
            return buildNumber;
        }

        static void saveBuildNumber(int number)
        {
            File.WriteAllText("Builds/Android/.build", "" + number);
        }
    }
}