using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Vivat
{
    public class DefinesEditor:EditorWindow
    {
        private const string CONFIG_PATH = "Assets/mcs.rsp";
        private List<string> defines; 
        private Vector2 scroll = Vector2.zero;

        [MenuItem("Tools/DefinesEditor")]
        public static void show()
        {
            EditorWindow.GetWindow(typeof(DefinesEditor), false, "DefinesEditor");
        }

        void OnEnable()
        {
            parseDefines();
        }

        private void parseDefines()
        {
            defines = parseRspFile(CONFIG_PATH);
        }

        private List<string> parseRspFile(string path)
        {
            if(!File.Exists(path))
            {
                return new List<string>();
            }

            string[] lines = File.ReadAllLines(path);
            List<string> defs = new List<string>();

            foreach(string line in lines)
            {
                if(line.StartsWith("-define:"))
                {
                    defs.AddRange(line.Replace("-define:", "").Split(';'));
                }
            }

            return defs;
        }

        private void OnGUI()
        {
            GUILayout.Label("Defines");

            scroll = GUILayout.BeginScrollView(scroll);

            int count = defines.Count;
            for(int i = 0; i < defines.Count; i++)
            {
                drawDefine(i);
            }

            GUILayout.Space(4);

            if(GUILayout.Button("Add"))
            {
                defines.Add("NEW_DEFINE");
            }

            if(GUILayout.Button("Save"))
            {
                saveRspFile(CONFIG_PATH, defines);
                // AssetDatabase.ImportAsset(DEF_MANAGER_PATH, ImportAssetOptions.ForceUpdate);
                parseDefines();
            }

            GUILayout.EndScrollView();
        }

        private void drawDefine(int i)
        {
            GUILayout.BeginHorizontal();
                    
            defines[i] = EditorGUILayout.TextField(defines[i]);
            if(GUILayout.Button("x", GUIStyle.none, GUILayout.MaxWidth(18)))
            {
                defines.RemoveAt(i);
            }

            GUILayout.EndHorizontal();
        }

        void saveRspFile(string path, List<string> defs)
        {
            if(defs.Count == 0 && File.Exists(path))
            {
                File.Delete(path);
                File.Delete(path + ".meta");
                AssetDatabase.Refresh();
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("-define:");
            
            int count = defs.Count;
            for(int i = 0; i < count; i++)
            {
                sb.Append(defs[i]);

                if(i < (count - 1)) 
                {
                    sb.Append(";");
                }
            }

            using(StreamWriter writer = new StreamWriter(path, false))
            {
                writer.Write(sb.ToString());
            }
        }
    }
}