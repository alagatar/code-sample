﻿using System.Collections;
using UnityEngine;

public class BaseMenu:MonoBehaviour
{
    [HideInInspector]
    public bool active = false;
    public Animator animator;
    public float disableTimeout = 0.5f;

    protected readonly int anim_show = Animator.StringToHash("show");
    protected readonly int anim_hide = Animator.StringToHash("hide");

    bool inited = false;

    public virtual void show()
    {
        active = true;
        StopCoroutine("waitAndDisable");

        if(!inited)
        {
            inited = true;
            init();
        }

        gameObject.SetActive(true);
        animator.SetTrigger(anim_show);
    }

    public virtual void init()
    {
    }

    public virtual void hide()
    {
        active = false;
        animator.SetTrigger(anim_hide);

        StopCoroutine("waitAndDisable");
        StartCoroutine("waitAndDisable");
    }

    IEnumerator waitAndDisable()
    {
        yield return new WaitForSeconds(disableTimeout);
        gameObject.SetActive(false);
    }
}