using UnityEngine;
using UnityEngine.Events;

#if USE_IAP
using UnityEngine.Purchasing;

namespace Vivat
{
    [System.Serializable]
    public struct ProductItem
    {
        public string id;
        public ProductType type;
    }

    [System.Serializable]
    public class MyStringEvent:UnityEvent<string>
    {
    }

    public class NativeIapp:MonoBehaviour, IStoreListener
    {
        public static NativeIapp instance;
        public ProductItem[] products;
        public MyStringEvent onPurchased;

        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
        
        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        void Start()
        {
            if(m_StoreController == null)
            {
                InitializePurchasing();
            }
        }
        
        public void InitializePurchasing() 
        {
            if(IsInitialized())
            {
                return;
            }
            
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            int countProducts = products.Length;
            for(int i = 0; i < countProducts; i++)
            {
                builder.AddProduct(products[i].id, products[i].type);
            }

            UnityPurchasing.Initialize(this, builder);
        }
                
        private bool IsInitialized()
        {
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }    
          
        public void purchaseProduct(string productId)
        {
            if(!IsInitialized())
            {
                //  Debug.Log("BuyProductID FAIL. Not initialized.");
                return;
            }

            Product product = m_StoreController.products.WithID(productId);
            
            if(product != null && product.availableToPurchase)
            {
                // Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                // Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        
        public void restorePurchases()
        {
            if(!IsInitialized())
            {
                // Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }
            
            if (Application.platform == RuntimePlatform.IPhonePlayer || 
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // Debug.Log("RestorePurchases started ...");
                
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions((result) => {
                    // Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
        }
        
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;
        }
                
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }
                
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
        {
            if(onPurchased != null)
            {
                onPurchased.Invoke(args.purchasedProduct.definition.id);
            }
            return PurchaseProcessingResult.Complete;
        }
                
        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            // Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }
    }
}
#endif