﻿using System;

namespace Vivat
{
    public class Job
    {
        public Action callback;

        public void run()
        {
            callback();
        }
    }
}