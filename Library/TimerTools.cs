﻿using System;
using UnityEngine;

namespace Vivat
{
    public class TimerTools
    {
        public static int getSpendSeconds(string id)
        {
            DateTime startDate = getStartDate(id);
            int seconds = (int)(DateTime.Now - startDate).TotalSeconds;
            return seconds;
        }

        public static DateTime getStartDate(string id)
        {
            string timerId = id + "_timer";

            string startDateStr = PlayerPrefs.GetString(timerId, "");
            if(startDateStr == "")
            {
                startDateStr = DateTime.Now.ToString();
                PlayerPrefs.SetString(timerId, startDateStr);
                return DateTime.Now;
            }

            DateTime date;
            if(!DateTime.TryParse(startDateStr, out date))
            {
                date = DateTime.Now;
            }
            return date;
        }

        public static bool checkTimerPreset(string id)
        {
            string timerId = id + "_timer";
            if(PlayerPrefs.GetString(timerId, "") == "")
            {
                return false;
            }
            return true;
        }

        public static void saveCurrentTime(string id, int shift = 0)
        {
            string timerId = id + "_timer";
            DateTime timerTime = DateTime.Now.AddSeconds(shift);
            PlayerPrefs.SetString(timerId, timerTime.ToString());
        }

        public static void shiftTime(string id, int seconds)
        {
            string timerId = id + "_timer";
            DateTime timerTime = getStartDate(id);
            timerTime = timerTime.AddSeconds(seconds);
            PlayerPrefs.SetString(timerId, timerTime.ToString());
        }
    }
}