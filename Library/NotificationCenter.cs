using System.Collections.Generic;

namespace Vivat
{
    //var args = new object[] {id, order};
    //NotificationCenter.sendNotify("activateCard", args);

    public interface NotifyListener
    {
        void onNotify(string eventId, params object[] args);
    }

    public class NotificationCenter
    {
        struct Target
        {
            public string eventId;
            public NotifyListener listener;
            public string group;
        }

        private static List<Target> items = new List<Target>();

        public static void addListener(string eventId, NotifyListener listener, string group = "local")
        {
            if(isPresent(eventId, listener))
            {
                return;
            }

            Target newTarget;
            newTarget.eventId = eventId;
            newTarget.listener = listener;
            newTarget.group = group;

            items.Add(newTarget);
        }

        public static bool isPresent(string eventId, NotifyListener listener)
        {
            int count = items.Count;
            for(int i = 0; i < count; i++)
            {
                if(items[i].eventId == eventId && 
                    items[i].listener == listener)
                {
                    return true;
                }
            }
            return false;
        }

        public static void removeListener(string eventId, NotifyListener listener)
        {
            int count = items.Count;
            for(int i = 0; i < count; i++)
            {
                if(items[i].eventId == eventId && 
                    items[i].listener == listener)
                {
                    items.RemoveAt(i);
                    break;
                }
            }
        }

        public static void sendNotify(string eventId, params object[] args)
        {
            int count = items.Count;
            for(int i = 0; i < count; i++)
            {
                if(items[i].eventId == eventId)
                {
                    items[i].listener.onNotify(eventId, args);
                }
            }
        }

        public static void clear(string group = "local")
        {
            int index = 0;
            int count = items.Count;
            while(index < count)
            {
                if(items[index].group == group)
                {
                    items.RemoveAt(index);
                    count--;
                }
                else
                {
                    index++;
                }
            }
        }

        public static void clearAll()
        {
            items.Clear();
        }
    }
}
