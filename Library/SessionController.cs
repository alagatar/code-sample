using UnityEngine;

namespace Vivat
{
    public class SessionController:MonoBehaviour
    {
        [Tooltip("Seconds")]
        public int timeout = 600;//s

        private bool sessionStarted = false;

        private void OnApplicationPause(bool pauseStatus)
        {
            if(pauseStatus)
            {
                onStopSession();
            }
            else
            {
                onStartSession();
            }
        }

        private void OnApplicationQuit()
        {
            onStopSession();
        }

        private void onStartSession()
        {
            if(sessionStarted)
            {
                return;
            }
            sessionStarted = true;

            int spend = TimerTools.getSpendSeconds("session");
            if(spend > timeout)
            {
                completeSession();
            }

            TimerTools.saveCurrentTime("session");
        }

        private void completeSession()
        {
            NotificationCenter.sendNotify("completeSession");
        }

        private void onStopSession()
        {
            if(!sessionStarted)
            {
                return;
            }
            sessionStarted = false;

            TimerTools.saveCurrentTime("session");
        }
    }
}