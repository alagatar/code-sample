﻿using UnityEngine;

namespace Vivat
{
    [CreateAssetMenu(fileName = "VariablesData", menuName = "Custom/Variables")]
    public class Variables:ScriptableObject
    {
        [Header("Gift")]
        public int giftTimeout;

        [Header("Menu ads")]
        public int menuAdsCoinsMin;
        public int menuAdsCoinsMax;

        [Header("Score controller")]
        public int scorePerBlock;
        public int scorePerBlockToSkills;

        [Header("Rank panel")]
        public int[] rankRiseList;
        public int rankRiseValue;

        [Header("Score mul controller")]
        public int multiplierMax;
        public int multiplierMaxProgress;
        public float multiplierDecreaseStep;
        public float multiplierDecreaseTimeout;

        [Header("Skill generate coins")]
        public int generateCoinsScore;
        public int generateCoinsMin;
        public int generateCoinsMax;

        [Header("Skill clear layer")]
        public int clearLayerScore;

        [Header("Result menu")]
        public int reviveByCoinsStart;

        [Header("Rank menu gift")]
        public int rankGift;
    }
}