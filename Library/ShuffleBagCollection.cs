﻿using System;
using System.Collections.Generic;

namespace Vivat
{
    public class ShuffleBagCollection<T>
    {
        List<T> data;
        int cursor;
        private Random random;

        public ShuffleBagCollection()
        {
            data = new List<T>();
            cursor = -1;
            random = new Random();
        }

        public void add(T item, int quantity = 1, bool upCursor = true)
        {
            for(int i = 0; i < quantity; i++)
            {
                data.Add(item);
            }

            if(upCursor)
            {
                cursor = data.Count - 1;
            }
        }

        public T next()
        {
            if(cursor < 1)
            {
                cursor = data.Count - 1;
                return data[0];
            }

            int index = random.Next(cursor);
            T current = data[index];
            data[index] = data[cursor];
            data[cursor] = current;
            cursor--;
            return current;
        }

        public void clear()
        {
            data.Clear();
        }

        public void reset()
        {
            cursor = data.Count - 1;
        }

        public void insertBeforCursor(T item, int quantity = 1)
        {
            for(int i = 0; i < quantity; i++)
            {
                data.Insert(cursor, item);
            }

            cursor += quantity;
        }
    }
}