﻿[System.Serializable]
public struct IntPoint
{
    public int x;
    public int y;

    public IntPoint(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override string ToString()
    {
        return string.Format("x:{0} y:{1}", x,y);
    }
}