﻿using UnityEngine;

namespace Vivat
{
    [System.Serializable]
    public class Achievement
    {
        public string id;
        public AchievementCondition[] conditions;
        public bool earned;

        public void init()
        {
            initConditions();
            checkEarned();
        }

        void initConditions()
        {
            int count = conditions.Length;
            for(int i = 0; i < count; i++)
            {
                conditions[i].init();
            }
        }

        void checkEarned()
        {
            earned = true;
            int count = conditions.Length;
            for(int i = 0; i < count; i++)
            {
                if(!conditions[i].complete)
                {
                    earned = false;
                    break;
                }
            }
        }

        public bool checkPresentCondition(string statistic)
        {
            int count = conditions.Length;
            for(int i = 0; i < count; i++)
            {
                if(conditions[i].statistic == statistic)
                {
                    return true;
                }
            }
            return false;
        }

        public void trigger(string statistic, int value)
        {
            int count = conditions.Length;
            AchievementCondition condition;
            for(int i = 0; i < count; i++)
            {
                condition = conditions[i];
                if(!condition.complete)
                {
                    condition.trigger(value);
                }
            }

            checkEarned();

            float percent = getCompletePercent() * 100f;
            NotificationCenter.sendNotify("updateAchievement", id, percent);
        }

        public float getCompletePercent()
        {
            int total = 0;
            int current = 0;

            int count = conditions.Length;
            AchievementCondition condition;
            for(int i = 0; i < count; i++)
            {
                condition = conditions[i];
                total += condition.count;
                current += condition.current;

            }
            
            return Tools.toPercent(current, total);
        }
    }
}