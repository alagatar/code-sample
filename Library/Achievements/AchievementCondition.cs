﻿using UnityEngine;

namespace Vivat
{
    [System.Serializable]
    public class AchievementCondition
    {
        public string statistic;
        public int count;
        public int current;
        public bool complete;

        public void init()
        {
            current = PlayerPrefs.GetInt(statistic);
            if(current >= count)
            {
                complete = true;
            }
            else
            {
                complete = false;
            }
        }

        public void trigger(int value)
        {
            current = value;
            if(current >= count)
            {
                complete = true;
            }
        }
    }
}