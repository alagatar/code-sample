﻿using UnityEngine;

namespace Vivat
{
    public class AchievementManager:MonoBehaviour
    {
        public static AchievementManager instance;

        public AchievementsConfig config;

        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);

                init();
            }
        }

        void init()
        {
            config.init();
        }

        public void trigger(string statistic, int count = 1)
        {
            int current = PlayerPrefs.GetInt(statistic);
            current += count;
            PlayerPrefs.SetInt(statistic, current);

            config.trigger(statistic, current);
        }
    }
}