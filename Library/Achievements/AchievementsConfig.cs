﻿using UnityEngine;

namespace Vivat
{
    [CreateAssetMenu(fileName = "AchievementsConfigData", menuName = "Custom/AchievementsConfig")]
    public class AchievementsConfig:ScriptableObject
    {
        public Achievement[] achievements;

        public void init()
        {
            int count = achievements.Length;
            for(int i = 0; i < count; i++)
            {
                achievements[i].init();
            }
        }

        public void trigger(string statistic, int value)
        {
            int count = achievements.Length;
            Achievement achievement;
            for(int i = 0; i < count; i++)
            {
                achievement = achievements[i];
                if(!achievement.earned && 
                    achievement.checkPresentCondition(statistic))
                {
                    achievement.trigger(statistic, value);
                }
            }
        }
    }
}