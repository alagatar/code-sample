﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivat
{
    public class JobQueue:MonoBehaviour, NotifyListener
    {
        public int count;

        List<Job> items;


        void Start()
        {
            items = new List<Job>();
            count = 0;
            NotificationCenter.addListener("jobEvent", this);
        }

        public void onNotify(string eventId, params object[] args)
        {
            onJobEvent(args);
        }

        void onJobEvent(object[] args)
        {
            string action = (string)args[0];
            if(action == "new")
            {
                add(args);
            }
            else
            {
                complete();
            }
        }

        public void add(object[] args)
        {
            Job newJob = (Job)args[1];
            items.Add(newJob);

            count = items.Count;
            if(count == 1)
            {
                newJob.run();
            }
        }

        void complete()
        {
            items.RemoveAt(0);
            count = items.Count;
            next();
        }

        void next()
        {
            if(items.Count == 0)
            {
                return;
            }

            Job firstJob = items[0];
            firstJob.run();
        }
    }
}