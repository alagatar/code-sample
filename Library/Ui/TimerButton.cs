﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Vivat
{
    public class TimerButton:MonoBehaviour
    {
        public string id;
        public int[] timeouts;
        public int timeoutId;
        public int currentTimeout;
        public Text timerField;
        public bool completed = false;
        public Action completeEvent;

        DateTime startDate;

        private void Start()
        {
            initTimeout();
            updateField();

            if(!completed)
            {
                StartCoroutine("updateTimer");
            }
        }

        private void initTimeout()
        {
            startDate = getStartDate();

            string timeoutIdStr = id + "_id";
            timeoutId = PlayerPrefs.GetInt(timeoutIdStr, 0);
            if(timeoutId >= timeouts.Length)
            {
                timeoutId = 0;
            }
            currentTimeout = timeouts[timeoutId];
            completed = isComplete();
        }

        private DateTime getStartDate()
        {
            string startId = id + "_start";
            string startDateStr = PlayerPrefs.GetString(startId, "");
            if(startDateStr == "")
            {
                startDateStr = DateTime.Now.ToString();
                PlayerPrefs.SetString(startId, startDateStr);
            }

            DateTime date;
            if(!DateTime.TryParse(startDateStr, out date))
            {
                date = DateTime.Now;
            }
            return date;
        }

        public bool isComplete()
        {
            double spend = getSpendSeconds();
            if(spend >= currentTimeout)
            {
                return true;
            }
            return false;
        }

        public bool checkFullComplete()
        {
            initTimeout();
            return completed;
        }

        private int getSpendSeconds()
        {
            return (int)(DateTime.Now - startDate).TotalSeconds;
        }

        private IEnumerator updateTimer()
        {
            while(true)
            {
                yield return new WaitForSeconds(1f);
                if(isComplete())
                {
                    completeTimer();
                }
                updateField();
            }
        }

        private void completeTimer()
        {
            completed = true;
            StopCoroutine("startTimer");

            if(completeEvent != null)
            {
                completeEvent();
            }
        }

        private void updateField()
        {
            if(completed)
            {
                timerField.gameObject.SetActive(false);
                return;
            }
            else
            {
                timerField.gameObject.SetActive(true);

                int leaveSeconds = getLeaveSeconds();
                timerField.text = formatTime(leaveSeconds);
            }
        }

        private int getLeaveSeconds()
        {
            return currentTimeout - getSpendSeconds();
        }

        private string formatTime(int seconds)
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            return string.Format("{0:00}:{1:00}", time.Minutes, time.Seconds);
        }    

        public void startTimer()
        {
            saveCurrentTime();
            nextTimeout();
            updateField();

            StartCoroutine("updateTimer");
        }

        void saveCurrentTime()
        {
            startDate = DateTime.Now;
            string startId = id + "_start";
            string startDateStr = startDate.ToString();
            PlayerPrefs.SetString(startId, startDateStr);
        }

        private void nextTimeout()
        {
            timeoutId++;
            if(timeoutId >= timeouts.Length)
            {
                timeoutId = 0;
            }
            string timeoutIdStr = id + "_id";
            PlayerPrefs.SetInt(timeoutIdStr, timeoutId);

            initTimeout();
        }

        private void OnDestroy()
        {
            StopCoroutine("updateTimer");
        }
    }
}