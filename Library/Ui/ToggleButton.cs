﻿using UnityEngine;
using UnityEngine.UI;

namespace Vivat
{
    public class ToggleButton:MonoBehaviour
    {
        public Image image;
        public Sprite onState;
        public Sprite offState;
        
        [SerializeField]
        private bool isChecked = true;

        private void Awake()
        {
            image.sprite = onState;
        }

        public void setChecked(bool mode)
        {
            if(isChecked == mode)
            {
                return;
            }

            isChecked = mode;

            if(isChecked)
            {
                image.sprite = onState;
            }
            else
            {
                image.sprite = offState;
            }
        }
    }
}