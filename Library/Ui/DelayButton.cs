﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;

namespace Vivat
{
    public class DelayButton:MonoBehaviour
    {
        public float delay = 0.1f;
        public UnityEvent onStartEvent;
        public UnityEvent onCompleteEvent;

        void Awake()
        {
            GetComponent<Button>().onClick.AddListener(() => OnClick());
        }

        void OnDestroy()
        {
            GetComponent<Button>().onClick.RemoveListener(() => OnClick());
        }

        public void OnClick()
        {
            if(onStartEvent != null)
            {
                onStartEvent.Invoke();
            }

            SoundManager.instance.playSfx("c1");
            StartCoroutine("StartAnim");
        }

        IEnumerator StartAnim()
        {
            yield return new WaitForSeconds(delay);
            CompleteAnim();
        }

        void CompleteAnim()
        {
            if(onCompleteEvent != null)
            {
                onCompleteEvent.Invoke();
            }
        }
    }
}