﻿using UnityEngine;

namespace Vivat
{
    public class SoundButton:MonoBehaviour
    {
        public ToggleButton toggleButton;

        [SerializeField]
        private bool active = true;

        void Start()
        {
            active = !SoundManager.instance.isMuted();
            toggleButton.setChecked(active);
        }

        public void onClick()
        {
            active = !active;
            toggleButton.setChecked(active);

            SoundManager.instance.muteAll(!active);
        }
    }
}