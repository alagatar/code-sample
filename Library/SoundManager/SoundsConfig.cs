﻿using System.Collections.Generic;
using UnityEngine;

namespace Vivat
{
    [CreateAssetMenu(fileName = "SoundsConfigData", menuName = "Custom/SoundsConfig")]
    public class SoundsConfig:ScriptableObject
    {
        [SerializeField]
        public List<SoundConfig> sounds;

        public SoundConfig find(string id)
        {
            int count = sounds.Count;
            for(int i = 0; i < count; i++)
            {
                if(sounds[i].name == id)
                {
                    return sounds[i];
                }
            }
            return null;
        }
    }
}