﻿using UnityEngine;

namespace Vivat
{
    [System.Serializable]
    public class SoundConfig
    {
        public AudioClip clip;
        public string name;
        [Range(0f, 1f)]
        public float volume = 1f;
    }
}