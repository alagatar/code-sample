﻿using System.Collections;
using UnityEngine;

namespace Vivat
{
    public class SoundNode:MonoBehaviour
    {
        public string activeClipId;
        public SoundGroup group;
        public string chanel;
        public bool looped;
        public bool active;
        public bool muted;
        public bool baseActive = true;

        AudioSource source;
        float baseVolume = 1f;
        float volume = 1f;

        public void init(SoundGroup group, string chanel, float baseVolume, bool baseMuted)
        {
            active = false;
            looped = false;
            muted = baseMuted;

            this.group = group;
            this.chanel = chanel;
            this.baseVolume = baseVolume;

            source = gameObject.AddComponent<AudioSource>();
            source.playOnAwake = false;
            source.mute = muted;
        }

        public void play(SoundConfig config, bool looped, bool baseActive)
        {
            active = true;
            this.baseActive = baseActive;

            activeClipId = config.name;
            gameObject.name = "SoundNode(" + activeClipId + ")";

            this.looped = looped;
            this.volume = config.volume;

            source.clip = config.clip;
            source.volume = baseVolume * volume;
            if(!this.baseActive)
            {
                source.volume = 0f;
            }
            source.loop = looped;
            source.Play();

            if(!looped)
            {
                StartCoroutine("waitComplete");
            }
        }

        IEnumerator waitComplete()
        {
            float length = source.clip.length;
            while(true)
            {
                yield return null;
                
                if(source.time == 0)
                {
                    completePlay();
                    break;
                }
            }
        }

        void completePlay()
        {
            active = false;
        }

        public void mute(bool mode)
        {
            if(muted == mode)
            {
                return;
            }
            muted = mode;
            source.mute = muted;
        }

        public void setBaseVolume(float value)
        {
            baseVolume = value;
            source.volume = baseVolume * volume;
        }

        public void setBaseActive(bool mode)
        {
            if(baseActive == mode)
            {
                return;
            }
            baseActive = mode;

            if(baseActive)
            {
                source.volume = baseVolume * volume;
            }
            else
            {
                source.volume = 0f;
            }
        }
    }
}