using System.Collections.Generic;
using UnityEngine;

namespace Vivat
{
    public enum SoundGroup
    {
        Sfx,
        Music
    };

    public class SoundManager : MonoBehaviour
    {
        public static SoundManager instance;

        public bool active = true;
        public bool sfxMuted;
        public bool musicMuted;
        [Range(0.0f, 1.0f)]
        public float sfxVolume = 1f;
        [Range(0.0f, 1.0f)]
        public float musicVolume = 1f;
        public SoundsConfig config;

        List<SoundNode> nodes;

        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
                init();
            }
        }

        void init()
        {
            sfxVolume = PlayerPrefs.GetFloat("sfxVolume", 1f);
            musicVolume = PlayerPrefs.GetFloat("musicVolume", 1f);

            if(PlayerPrefs.GetInt("sfxMuted", 0) == 1)
            {
                sfxMuted = true;
            }
            else
            {
                sfxMuted = false;
            }

            if(PlayerPrefs.GetInt("musicMuted", 0) == 1)
            {
                musicMuted = true;
            }
            else
            {
                musicMuted = false;
            }

            nodes = new List<SoundNode>();
        }

        public void playSfx(string id, bool looped = false)
        {
            playMedia(id, looped, SoundGroup.Sfx);
        }

        public void playMusic(string id, bool looped = false)
        {
            playMedia(id, looped, SoundGroup.Music);
        }

        public void playMedia(string id, bool looped, SoundGroup group, string chanel = "sounds")
        {
            SoundConfig madia = config.find(id);
            if(madia == null)
            {
                return;
            }

            SoundNode node = getSoundNode(group, chanel);
            node.play(madia, looped, active);
        }

        SoundNode getSoundNode(SoundGroup group, string chanel)
        {
            SoundNode newNode = findSoundNode(group, chanel);
            if(newNode == null)
            {
                newNode = createSoundNode(group, chanel);
            }
            return newNode;
        }

        SoundNode findSoundNode(SoundGroup group, string chanel)
        {
            int count = nodes.Count;
            for (int i = 0; i < count; i++)
            {
                if(!nodes[i].active && 
                    nodes[i].group == group && 
                    nodes[i].chanel == chanel)
                {
                    return nodes[i];
                }
            }
            return null;
        }

        SoundNode createSoundNode(SoundGroup group, string chanel)
        {
            GameObject container = new GameObject();
            container.transform.parent = transform;

            SoundNode newNode = container.AddComponent<SoundNode>();

            float volume;
            bool muted;
            if(group == SoundGroup.Sfx)
            {
                volume = sfxVolume;
                muted = sfxMuted;
            }
            else
            {
                volume = musicVolume;
                muted = musicMuted;
            }

            newNode.init(group, chanel, volume, muted);
            nodes.Add(newNode);

            return newNode;
        }

        public void mute(bool mode, SoundGroup group)
        {
            if(group == SoundGroup.Sfx)
            {
                if(sfxMuted == mode)
                {
                    return;
                }
                sfxMuted = mode;

                int modeVal = 0;
                if(sfxMuted)
                {
                    modeVal = 1;
                }
                PlayerPrefs.SetInt("sfxMuted", modeVal);
            }
            else
            {
                if(musicMuted == mode)
                {
                    return;
                }
                musicMuted = mode;

                int modeVal = 0;
                if(musicMuted)
                {
                    modeVal = 1;
                }
                PlayerPrefs.SetInt("musicMuted", modeVal);
            }

            int count = nodes.Count;
            SoundNode node;
            for(int i = 0; i < count; i++)
            {
                node = nodes[i];
                if(node.group == group)
                {
                    node.mute(mode);
                }
            }
        }
        
        public bool isMuted()
        {
            return sfxMuted && musicMuted;
        }

        public void muteAll(bool mode)
        {
            mute(mode, SoundGroup.Music);
            mute(mode, SoundGroup.Sfx);
        }

        public void changeVolume(SoundGroup group, float value)
        {
            value = Mathf.Clamp(value, 0f, 1f);

            if(group == SoundGroup.Sfx)
            {
                sfxVolume = value;
                PlayerPrefs.SetFloat("sfxVolume", value);
            }
            else
            {
                musicVolume = value;
                PlayerPrefs.SetFloat("musicVolume", value);
            }

            int count = nodes.Count;
            SoundNode node;
            for(int i = 0; i < count; i++)
            {
                node = nodes[i];
                if(node.group == group)
                {
                    node.setBaseVolume(value);
                }
            }
        }

        public void setActive(bool mode)
        {
            if(active == mode)
            {
                return;
            }
            active = mode;

            int count = nodes.Count;
            for(int i = 0; i < count; i++)
            {
                nodes[i].setBaseActive(mode);
            }
        }

        public bool isMusicPlaying(string id)
        {
            int count = nodes.Count;
            SoundNode node;
            for(int i = 0; i < count; i++)
            {
                node = nodes[i];
                if(node.group == SoundGroup.Music && 
                    node.activeClipId == id && 
                    node.active == true)
                {
                    return true;
                }
            }
            return false;
        }
    }
}