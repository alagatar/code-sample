﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Vivat
{ 
    [System.Serializable]
    public struct CoroutineCallback
    {
        public MonoBehaviour target;
        public string coroutineName;
    }

    public class SceneLoadManager:MonoBehaviour
    {
        public static SceneLoadManager instance;

        public GameObject canvasContainer;
        public CanvasGroup loadContainer;
        public float fadeInSpeed = 1.0f;
        public float fadeOutSpeed = 2.0f;
        public bool cleanObjectPool = false;

        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }  
        }

        public void loadScene(string sceneName)
        {
            StartCoroutine(fadeAndLoadScene(sceneName));
        }

        IEnumerator fadeAndLoadScene(string sceneName)
        {
            canvasContainer.SetActive(true);
            loadContainer.blocksRaycasts = true;

            yield return StartCoroutine(fadeIn());

            if(cleanObjectPool)
            {
                yield return ObjectPoolManager.instance.clean();
            }

            yield return SceneManager.LoadSceneAsync(sceneName);
            yield return null;
            yield return StartCoroutine(fadeOut());

            loadContainer.blocksRaycasts = false;
            canvasContainer.SetActive(false);
        }

        IEnumerator fadeIn()
        {
            float alpha = 0f;
            loadContainer.alpha = alpha;
            while(alpha < 1)
            {
                alpha += fadeInSpeed * Time.deltaTime;
                loadContainer.alpha = alpha;
                yield return null;
            }
            loadContainer.alpha = 1f;
            yield break;
        }

        IEnumerator fadeOut()
        {
            float alpha = 1f;
            loadContainer.alpha = alpha;
            while(alpha > 0)
            {
                alpha -= fadeOutSpeed * Time.deltaTime;
                loadContainer.alpha = alpha;
                yield return null;
            }
            loadContainer.alpha = 0f;
            yield break;
        }
    }
}