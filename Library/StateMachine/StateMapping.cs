using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vivat
{
    public class StateMapping
    {
        public bool hasStartRoutine;
		public Action startAction;
		public Func<IEnumerator> startRoutine;

        public Action updateAction;

        public bool hasEndRoutine;
		public Action endAction;
		public Func<IEnumerator> endRoutine;
    }
}
