using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Vivat
{
    public enum StateStep
    {
        Start,
        Update,
        End
    }

    public class StateMachine<T> where T : struct
    {
        public T prevState;
        public T state;
        public Action<T> onChangeState;

        private MonoBehaviour target;
        private Dictionary<T, StateMapping> statesMapping;
        private IEnumerator enterRoutine;
        private IEnumerator exitRoutine;
        private bool inTransition = false;
		private StateMapping currentStateMap;

        public StateMachine(MonoBehaviour target)
        {
            this.target = target;
            initMapping();
        }

        private void initMapping()
        {
            statesMapping = new Dictionary<T, StateMapping>();

            MethodInfo[] targetMethods = target.GetType().GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public |
									  BindingFlags.NonPublic);

            var values = Enum.GetValues(typeof(T));
            int count = values.Length;
            if(count == 1)
            {
                return;
            }

            for(int i = 1; i < count; i++)
            {
                addHandlesForState((T)values.GetValue(i), targetMethods);
            }
        }

        private void addHandlesForState(T state, MethodInfo[] targetMethods)
        {
            StateMapping map = new StateMapping();
            
            addHandlerFor(map, state, StateStep.Start, targetMethods);
            addHandlerFor(map, state, StateStep.Update, targetMethods);
            addHandlerFor(map, state, StateStep.End, targetMethods);

            statesMapping.Add(state, map);
        }

        private void addHandlerFor(StateMapping map, T state, StateStep step, MethodInfo[] targetMethods)
        {
            string handleName = step.ToString().ToLower() + state.ToString() + "State";

            MethodInfo method = getMethodByNameFrom(targetMethods, handleName);
            if(method != null)
            {
                switch(step)
                {
                    case StateStep.Start:
                        if(method.ReturnType == typeof(IEnumerator))
                        {
                            map.hasStartRoutine = true;
                            map.startRoutine = CreateDelegate<Func<IEnumerator>>(method, target);
                        }
                        else
                        {
                            map.hasStartRoutine = false;
                            map.startAction = CreateDelegate<Action>(method, target);
                        }
                    break;

                    case StateStep.Update:
                        map.updateAction = CreateDelegate<Action>(method, target);
                    break;

                    case StateStep.End:
                        if(method.ReturnType == typeof(IEnumerator))
                        {
                            map.hasEndRoutine = true;
                            map.endRoutine = CreateDelegate<Func<IEnumerator>>(method, target);
                        }
                        else
                        {
                            map.hasEndRoutine = false;
                            map.endAction = CreateDelegate<Action>(method, target);
                        }
                    break;
                }
            }
        }

        private MethodInfo getMethodByNameFrom(MethodInfo[] targetMethods, string handleName)
        {
            int count = targetMethods.Length;
            for(int i = 0; i < count; i++)
            {
                if(targetMethods[i].GetCustomAttributes(typeof(CompilerGeneratedAttribute), true).Length != 0)
				{
					continue;
				}

                if(targetMethods[i].Name == handleName)
                {
                    return targetMethods[i];
                }
            }
            return null;
        }

        private V CreateDelegate<V>(MethodInfo method, UnityEngine.Object target) where V : class
		{
			var ret = Delegate.CreateDelegate(typeof(V), target, method) as V;
			if(ret == null)
			{
				throw new ArgumentException("Unabled to create delegate for method called " + method.Name);
			}
			return ret;
		}

        public void setInitState(T state)
        {
            this.state = state;
            prevState = state;

            currentStateMap = statesMapping[state];
            if(currentStateMap.hasStartRoutine)
            {
                target.StartCoroutine(initCoroutine());            
            }
            else
            {
                currentStateMap.startAction();
            }
        }

        private IEnumerator initCoroutine()
        {
            inTransition = true;

            enterRoutine = currentStateMap.startRoutine();
            yield return target.StartCoroutine(enterRoutine);
            enterRoutine = null;

            inTransition = false;
        }

        public void changeState(T nextState)
        {
            if(nextState.Equals(state))
            {
                return;
            }

            if(onChangeState != null)
            {
                onChangeState(nextState);
            }

            target.StartCoroutine(changeCoroutine(nextState));
        }

        private IEnumerator changeCoroutine(T nextState)
        {
            //Wait transition
            while(inTransition)
            {
                yield return null;
            }

            inTransition = true;

            //Complete current state
            if(currentStateMap.hasEndRoutine)
            {
                exitRoutine = currentStateMap.endRoutine();
                yield return target.StartCoroutine(exitRoutine);
                exitRoutine = null;
            }
            else
            {
                currentStateMap.endAction();
            }

            //Start new state
            prevState = state;
            state = nextState;
            currentStateMap = statesMapping[state];
            if(currentStateMap.hasStartRoutine)
            {
                enterRoutine = currentStateMap.startRoutine();
                yield return target.StartCoroutine(enterRoutine);
                enterRoutine = null;
            }
            else
            {
                currentStateMap.startAction();
            }
                        
            inTransition = false;
        }

        public void update()
        {
            if(inTransition)
            {
                return;
            }

            if(currentStateMap.updateAction != null)
            {
                currentStateMap.updateAction();
            }
        }
    }
}
