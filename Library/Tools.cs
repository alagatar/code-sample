﻿using System;
using System.Collections;
using UnityEngine;

namespace Vivat
{
    public class Tools
    {
        public static void waitFrame(MonoBehaviour container, Action callback)
        {
            container.StartCoroutine(waitFrameCoroutine(callback));
        }

        private static IEnumerator waitFrameCoroutine(Action callback)
        {
            yield return null;

            if(callback != null)
            {
                callback();
            }
        }

        public static int randomIntDivisible(int min, int max, int divisible = 5)
        {
            int randomValue = UnityEngine.Random.Range(min, max);
            int overValue = randomValue % divisible;
            randomValue -= overValue;
            return randomValue;
        }

        public static int randomIntExclude(int min, int max, int exclude)
        {
            int result = exclude;
            int maxIteration = 20;
            int iterations = 0;
            while(result == exclude)
            {
                iterations++;
                result = UnityEngine.Random.Range(min, max);

                if(iterations >= maxIteration)
                {
                    break;
                }
            }
            return result;
        }

        public static float toPercent(float current, float total)
        {
            return (current / total);
        }

        public static float toPercentDsc(float current, float total)
        {
            return 1 - (current / total);
        }

        public static float fromPercent(float percent, float total)
        {
            return (percent * total);
        }
    }
}