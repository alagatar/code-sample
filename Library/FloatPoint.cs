﻿[System.Serializable]
public struct FloatPoint
{
    public float x;
    public float y;

    public override string ToString()
    {
        return string.Format("x:{0} y:{1}", x,y);
    }
}