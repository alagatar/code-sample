public struct AdItem
{
    public static int INTERSTITIAL = 1;
    public static int REWARDED = 2;
    public static int VIDEO = 3;

    public int type;
    public string id;

    public AdItem(int _type, string _id)
    {
        type = _type;
        id = _id;
    }
}