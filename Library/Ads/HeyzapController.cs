using UnityEngine;
using System.Collections.Generic;
using System;

#if USE_HEYZAP
using Heyzap;

namespace Vivat
{
    public class HeyzapController:MonoBehaviour
    {
        public static HeyzapController instance;
        public string publisherId;
        public bool adsEnabled;

        bool inited;
        bool started;
        List<AdItem> adsOrder;
        bool sessionStarted = false;

        const int INTERSTITIAL = 1;
        const int REWARDED = 2;
        const int TIMEOUT = 120;//s
        const int SESSION_TIMEOUT = 600;//s

        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);

                if(!inited)
                {
                    init();
                }
            }
        }

        void init()
        {
            inited = true;
            if(PlayerPrefs.GetInt("noAds", 0) == 0)
            {
                adsEnabled = true;
            }
            else
            {
                adsEnabled = false;
            }
        }

        void Start()
        {
            if(!started)
            {
                onStart();
            }
        }

        void onStart()
        {
            started = true;
            adsOrder = new List<AdItem>();   

            HeyzapAds.Start(publisherId, HeyzapAds.FLAG_DISABLE_AUTOMATIC_FETCHING);
            initDelegates();

            if(adsEnabled)
            {
                showBanner();
                HZInterstitialAd.Fetch();
            }
            
            HZIncentivizedAd.Fetch();

            if(!TimerTools.checkTimerPreset("interstitialTimeout"))
            {
                TimerTools.saveCurrentTime("interstitialTimeout");
            }

            HeyzapAds.ShowMediationTestSuite();
        }

        void initDelegates()
        {
            HZInterstitialAd.AdDisplayListener listener = delegate(string adState, string adTag) {
            if(adState == "show")
            {
                onStartAd();
            }

            if(adState == "hide")
            {
                onCompleteAd();
            }

            //if(adState.Equals("failed"))
            //{
            //}

            //if(adState.Equals("available"))
            //{
            //    onAdLoaded();
            //}

            //if(adState.Equals("fetch_failed"))
            //{
            //}

            //if(adState.Equals("audio_starting"))
            //{
            //    onStartSound();
            //}

            //if(adState.Equals("audio_finished"))
            //{
            //    onStopSound();
            //}
            };
            HZInterstitialAd.SetDisplayListener(listener);

            HZIncentivizedAd.AdDisplayListener listener2 = delegate(string adState, string adTag) {
            if(adState == "incentivized_result_complete")
            {
                onRewardedComplete();
            }

            if(adState == "incentivized_result_incomplete")
            {
                onRewardedSkip();
            }
            };
            HZIncentivizedAd.SetDisplayListener(listener2);
        }

        void onStartAd()
        {
            SoundManager.instance.setActive(false);

            string adId = adsOrder[0].id;
            NotificationCenter.sendNotify("adEvent", "start", adId);
        }

        void onCompleteAd()
        {
            SoundManager.instance.setActive(true);

            AdItem adItem = adsOrder[0];
            adsOrder.RemoveAt(0);

            if(adItem.type == INTERSTITIAL && adsEnabled)
            {
                HZInterstitialAd.Fetch();
            }
            else if(adItem.type == REWARDED)
            {
                HZIncentivizedAd.Fetch();
            }

            if(adItem.type == REWARDED)
            {
                triggerResetInterstitialAds();
            }
            
            NotificationCenter.sendNotify("adEvent", "end", adItem.id);
        }

        //void onAdLoaded()
        //{
        //}

        //void onStartSound()
        //{
        //}

        //void onStopSound()
        //{
        //}

        void onRewardedComplete()
        {
            onCompleteAd();
        }

        void onRewardedSkip()
        {
            onCompleteAd();
        }

        public bool checkInterstitialReady()
        {
            if(HZInterstitialAd.IsAvailable())
            {
            return true;
            }
            return false;
        }

        public bool checkRewardedReady()
        {
            if(HZIncentivizedAd.IsAvailable())
            {
            return true;
            }
            return false;
        }

        public void showBanner()
        {
            HZBannerShowOptions showOptions = new HZBannerShowOptions();
            showOptions.Position = HZBannerShowOptions.POSITION_BOTTOM;
            HZBannerAd.ShowWithOptions(showOptions);
        }

        public void hideBanner()
        {
            HZBannerAd.Hide();
        }

        public void showInterstitial(string id = "interstitial")
        {
            adsOrder.Add(new AdItem(INTERSTITIAL, id));
            HZInterstitialAd.Show();
            triggerResetInterstitialAds();
            HZInterstitialAd.Fetch();
        }

        public void showRewarded(string id = "rewarded")
        {
            adsOrder.Add(new AdItem(REWARDED, id));
            HZIncentivizedAd.Show();
            HZIncentivizedAd.Fetch();
        }

        public void disableAds()
        {
            adsEnabled = false;
            PlayerPrefs.SetInt("noAds", 1);
            HZBannerAd.Destroy();
        }

        public bool checkShowInterstitial()
        {
            if(adsEnabled && 
                checkInterstitialReady() && 
                checkInterstitialTimeout())
            {
                return true;
            }
            return false;
        }

        bool checkInterstitialTimeout()
        {
            int spend = TimerTools.getSpendSeconds("interstitialTimeout");
            if(spend >= TIMEOUT)
            {
                return true;
            }
            return false;
        }

        public void triggerInterstitialAds()
        {
            if(checkShowInterstitial())
            {
                showInterstitial();
            }
        }

        public void triggerResetInterstitialAds()
        {
            TimerTools.saveCurrentTime("interstitialTimeout");
        }

        public void triggerShiftInterstitialAds(int seconds)
        {
            TimerTools.shiftTime("interstitialTimeout", seconds);
        }

        void OnApplicationFocus(bool hasFocus)
        {
            if(hasFocus)
            {
                onStartSession();
            }
            else
            {
                onStopSession();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if(pauseStatus)
            {
                onStopSession();
            }
            else
            {
                onStartSession();
            }
        }

        void OnApplicationQuit()
        {
            onStopSession();
        }  

        void onStartSession()
        {
            if(sessionStarted)
            {
                return;
            }
            sessionStarted = true;

            int spend = TimerTools.getSpendSeconds("session");
            if(spend > SESSION_TIMEOUT)
            {
                triggerResetInterstitialAds();
            }

            TimerTools.saveCurrentTime("session");
        }

        void onStopSession()
        {
            if(!sessionStarted)
            {
                return;
            }
            sessionStarted = false;

            TimerTools.saveCurrentTime("session");
        }
    }
}
#endif