using UnityEngine;
using System.Collections.Generic;
using System.Collections;

#if USE_TAPDAQ
using Tapdaq;

namespace Vivat
{
    public class TapdaqController:MonoBehaviour, NotifyListener
    {
        public enum InterstitialType {Static, Video}
        
        public static TapdaqController instance;
        public string crossPromoTag = "app-launch";
        public string interstitialTag = "interstitials";
        public string videoTag = "video";
        public string rewardedTag = "rewarded-video";
        public TDBannerPosition bannerPosition = TDBannerPosition.Bottom;
        public InterstitialType interstitialType = InterstitialType.Static;
        public int interstitialTimeout = 120;//s
        public int interstitialCounter = 5;
        public bool adsEnabled;
        public float reloadDelay = 1f;
        
        private bool waitInterstitial = false;
        private bool waitVideo = false;
        private bool waitRewarded = false;
        private bool waitBanner = false;
        private bool waitCoroutineRunning = false;
        private List<AdItem> adsOrder;
        private int counter;
        private WaitForSeconds reloadDelayWaiter;

        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);

                init();
            }
        }

        void init()
        {
            if(PlayerPrefs.GetInt("noAds", 0) == 0)
            {
                adsEnabled = true;
            }
            else
            {
                adsEnabled = false;
            }

            // Debug.Log("Tapdaq. adsEnabled: " + adsEnabled);
            counter = PlayerPrefs.GetInt("interstitialCounter", 0);
            reloadDelayWaiter = new WaitForSeconds(reloadDelay);
            adsOrder = new List<AdItem>();
        }

        void OnEnable()
        {
            // Debug.Log("Tapdaq. OnEnable");
            TDCallbacks.TapdaqConfigLoaded += onTapdaqInited;
            TDCallbacks.AdAvailable += onAdLoaded;
            TDCallbacks.AdDidDisplay += onStartAd;
            TDCallbacks.AdClosed += onCompleteAd;
            TDCallbacks.AdNotAvailable += onFailLoadAd;
        }

        void OnDisable()
        {
            // Debug.Log("Tapdaq. OnDisable");
            TDCallbacks.TapdaqConfigLoaded -= onTapdaqInited;
            TDCallbacks.AdAvailable -= onAdLoaded;
            TDCallbacks.AdDidDisplay -= onStartAd;
            TDCallbacks.AdClosed -= onCompleteAd;
            TDCallbacks.AdNotAvailable -= onFailLoadAd;
        }

        void onTapdaqInited()
        {
            // Debug.Log("Tapdaq. onTapdaqInited");
            
            // Debug.Log("Tapdaq. LoadInterstitialWithTag: " + crossPromoTag);
    #if !UNITY_EDITOR
                AdManager.LoadInterstitialWithTag(crossPromoTag);
    #endif

            if(adsEnabled)
            {
                loadInterstitial();            
                showBanner();
            }

            // Debug.Log("Tapdaq. LoadRewardedVideoWithTag: " + rewardedTag);
    #if !UNITY_EDITOR
                AdManager.LoadRewardedVideoWithTag(rewardedTag);
    #endif
        }

        private void loadInterstitial()
        {
            if(interstitialType == InterstitialType.Static)
            {
                // Debug.Log("Tapdaq. LoadInterstitialWithTag: " + interstitialTag);
    #if !UNITY_EDITOR
                    AdManager.LoadInterstitialWithTag(interstitialTag);
    #endif
            }
            else
            {
                // Debug.Log("Tapdaq. LoadVideoWithTag: " + interstitialTag);
    #if !UNITY_EDITOR
                    AdManager.LoadVideoWithTag(interstitialTag);
    #endif
            }
        }

        void onAdLoaded(TDAdEvent e)
        {
            // Debug.Log("Tapdaq. onAdLoaded: " + e.tag);

            if(e.adType == "INTERSTITIAL" && e.tag == crossPromoTag)
            {
                // Debug.Log("Tapdaq. ShowInterstitial promotion);
    #if !UNITY_EDITOR
                    AdManager.ShowInterstitial(crossPromoTag);
    #endif
            }
            else if(e.adType == "BANNER" && adsEnabled)
            {
                // Debug.Log("Tapdaq. ShowBanner);
    #if !UNITY_EDITOR
                    AdManager.ShowBanner(bannerPosition);
    #endif
            }
        }

        void onStartAd(TDAdEvent e)
        {
            Debug.Log("Tapdaq. onStartAd: " + e.tag);

            if(adsOrder.Count == 0)
            {
                return;
            }

            SoundManager.instance.setActive(false);

            string adId = adsOrder[0].id;
            NotificationCenter.sendNotify("adEvent", "start", adId);
        }

        void onCompleteAd(TDAdEvent e)
        {
            // Debug.Log("Tapdaq. onCompleteAd: " + e.tag + " => " + adsOrder.Count);

            if(adsOrder.Count == 0)
            {
                return;
            }

            SoundManager.instance.setActive(true);

            AdItem adItem = adsOrder[0];
            adsOrder.RemoveAt(0);

            NotificationCenter.sendNotify("adEvent", "end", adItem.id);

            if(adsEnabled && e.tag == interstitialTag)
            {
                loadInterstitial();
            }
            else if(e.adType == "VIDEO")
            {
                // Debug.Log("Tapdaq. LoadVideoWithTag: " + videoTag);
    #if !UNITY_EDITOR
                    AdManager.LoadVideoWithTag(videoTag);
    #endif
            }
            else if(e.adType == "REWARD_AD")
            {
                // Debug.Log("Tapdaq. LoadRewardedVideoWithTag: " + rewardedTag);
    #if !UNITY_EDITOR
                    AdManager.LoadRewardedVideoWithTag(rewardedTag);
    #endif
            }  
        }

        void onFailLoadAd(TDAdEvent e)
        {
            // Debug.Log("Tapdaq. onFailLoadAd");

            if((e.adType == "INTERSTITIAL" || e.adType == "VIDEO") && e.tag == interstitialTag) 
            {
                if(adsEnabled && !waitInterstitial)
                {
                    waitInterstitial = true;
                }

                if(!waitCoroutineRunning)
                {
                    // Debug.Log("Tapdaq. onFailLoadAd. Start wait static: " + interstitialTag);
                    StartCoroutine("waitAndLoadAds");
                }
            } 
            else if(e.adType == "VIDEO" && e.tag == videoTag)
            {
                if(!waitVideo)
                {
                    waitVideo = true;
                }

                if(!waitCoroutineRunning)
                {
                    // Debug.Log("Tapdaq. onFailLoadAd. Start wait: " + videoTag);
                    StartCoroutine("waitAndLoadAds");
                }
            }
            else if(e.adType == "REWARD_AD" && e.tag == rewardedTag)
            {
                if(!waitRewarded)
                {
                    waitRewarded = true;
                }

                if(!waitCoroutineRunning)
                {
                    // Debug.Log("Tapdaq. onFailLoadAd. Start wait: " + rewardedTag);
                    StartCoroutine("waitAndLoadAds");
                }
            } 
            else if(e.adType == "BANNER") 
            {
                if(adsEnabled && !waitBanner)
                {
                    waitBanner = true;
                }

                if(!waitCoroutineRunning)
                {
                    // Debug.Log("Tapdaq. onFailLoadAd. Start wait: banner");
                    StartCoroutine("waitAndLoadAds");
                }
            }
        }

        IEnumerator waitAndLoadAds()
        {
            // Debug.Log("Tapdaq. WaitAndLoadAds");

            waitCoroutineRunning = true;
            yield return reloadDelayWaiter;

            if(waitInterstitial)
            {
                waitInterstitial = false;

                loadInterstitial();
            }

            if(waitVideo)
            {
                waitVideo = false;

                // Debug.Log("Tapdaq. LoadVideoWithTag: " + videoTag);
    #if !UNITY_EDITOR
                    AdManager.LoadVideoWithTag(videoTag);
    #endif
            }

            if(waitRewarded)
            {
                waitRewarded = false;

                // Debug.Log("Tapdaq. LoadRewardedVideoWithTag: " + rewardedTag);
    #if !UNITY_EDITOR
                    AdManager.LoadRewardedVideoWithTag(rewardedTag);
    #endif
            }

            if(waitBanner)
            {
                waitBanner = false;

                // Debug.Log("Tapdaq. RequestBanner");
    #if !UNITY_EDITOR
                    AdManager.RequestBanner(TDMBannerSize.TDMBannerStandard);
    #endif
            }

            waitCoroutineRunning = false;
        }

        void Start()
        {
            // Debug.Log("Tapdaq. Start");

    #if !UNITY_EDITOR
                AdManager.Init();
    #endif

            if(!TimerTools.checkTimerPreset("interstitialTimeout"))
            {
                TimerTools.saveCurrentTime("interstitialTimeout");
            }

            NotificationCenter.addListener("completeSession", this, "global");

    #if !UNITY_EDITOR
                // AdManager.LaunchMediationDebugger();
    #endif
        }

        public bool checkInterstitialReady()
        {
            if(interstitialType == InterstitialType.Static)
            {
    #if !UNITY_EDITOR
                    if(AdManager.IsInterstitialReadyWithTag(interstitialTag))
                    {
                        return true;
                    }
    #endif
            }
            else
            {
    #if !UNITY_EDITOR
                    if(AdManager.IsVideoReadyWithTag(interstitialTag))
                    {
                        return true;
                    }
    #endif
            }
            return false;
        }

        public bool checkVideoReady()
        {
    #if !UNITY_EDITOR
                if(AdManager.IsVideoReadyWithTag(videoTag))
                {
                    return true;
                }
    #endif
            return false;
        }

        public bool checkRewardedReady()
        {
    #if !UNITY_EDITOR
                if(AdManager.IsRewardedVideoReadyWithTag(rewardedTag))
                {
                    return true;
                }
    #endif
            return false;
        }

        public void showBanner()
        {
            // Debug.Log("Tapdaq. showBanner");
    #if !UNITY_EDITOR
                AdManager.RequestBanner(TDMBannerSize.TDMBannerStandard);
    #endif
        }

        public void hideBanner()
        {
    #if !UNITY_EDITOR
                AdManager.HideBanner();
    #endif
        }

        public void showInterstitial(string id = "interstitial")
        {
            triggerResetInterstitialAds();

            // Debug.Log("Tapdaq. showInterstitial: " + id);
            adsOrder.Add(new AdItem(AdItem.INTERSTITIAL, id));

            if(interstitialType == InterstitialType.Static)
            {
    #if !UNITY_EDITOR
                    AdManager.ShowInterstitial(interstitialTag);
    #endif
            }
            else
            {
    #if !UNITY_EDITOR
                    AdManager.ShowVideo(interstitialTag);
    #endif
            }
        }

        public void showRewarded(string id = "rewarded")
        {
            // Debug.Log("Tapdaq. showRewarded: " + id);
            adsOrder.Add(new AdItem(AdItem.REWARDED, id));

    #if !UNITY_EDITOR
                AdManager.ShowRewardVideo(rewardedTag);
    #endif
        }

        public void showVideo(string id = "video")
        {
            // Debug.Log("Tapdaq. showVideo: " + id);
            adsOrder.Add(new AdItem(AdItem.VIDEO, id));

    #if !UNITY_EDITOR
                AdManager.ShowVideo(videoTag);
    #endif
        }

        public void disableAds()
        {
            // Debug.Log("Tapdaq. disableAds");

            adsEnabled = false;
            PlayerPrefs.SetInt("noAds", 1);

    #if !UNITY_EDITOR
                AdManager.HideBanner();
    #endif
        }

        public bool checkShowInterstitial()
        {
            if(adsEnabled &&
                checkInterstitialReady() &&
                checkInterstitialTimeout())
            {
                return true;
            }
            return false;
        }

        bool checkInterstitialTimeout()
        {
            int spend = TimerTools.getSpendSeconds("interstitialTimeout");
            if(spend >= interstitialTimeout && 
                counter >= interstitialCounter)
            {
                return true;
            }
            return false;
        }

        public void triggerInterstitialAds()
        {
            counter++;
            if(checkShowInterstitial())
            {
                showInterstitial();
            }
        }

        public void triggerResetInterstitialAds()
        {
            counter = 0;
            PlayerPrefs.SetInt("interstitialCounter", 0);

            TimerTools.saveCurrentTime("interstitialTimeout");
        }

        public void triggerShiftInterstitialAds(int seconds)
        {
            TimerTools.shiftTime("interstitialTimeout", seconds);
        }

        public void onNotify(string eventId, params object[] args)
        {
            if(eventId == "completeSession")
            {
                triggerResetInterstitialAds();
            }
        }
    }
}
#endif