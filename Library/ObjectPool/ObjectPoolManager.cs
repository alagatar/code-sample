using System.Collections;
using UnityEngine;

namespace Vivat
{
    public class ObjectPoolManager:MonoBehaviour
    {
        public static ObjectPoolManager instance;

        public int iterationsPerFrame = 100;

        [SerializeField]
        private ObjectPool[] pools;

        void Awake()
        {
            if(instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        IEnumerator Start()
        {
            //print("Init ObjectPoolManager");

            int count = pools.Length;
            int iterations = 0;
            ObjectPool pool;
            for(int i = 0; i < count; i++)
            {
                pool = pools[i];
                pool.init(transform);
                if(pool.initCount > 0)
                {
                    yield return StartCoroutine(pools[i].preCacheItems(iterations, iterationsPerFrame, pool.initCount));
                    iterations = pools[i].iterations;
                }
            }
        }

        public GameObject pop(int poolId)
        {
            ObjectPool pool = getPool(poolId);
            if(pool != null)
            {
                return pool.pop();
            }
            return null;
        }

        ObjectPool getPool(int poolId)
        {
            int count = pools.Length;
            ObjectPool pool;
            for(int i = 0; i < count; i++)
            {
                pool = pools[i];
                if(pool.poolId == poolId)
                {
                    return pool;
                }
            }
            return null;
        }

        public void push(GameObject item, int poolId)
        {
            ObjectPool pool = getPool(poolId);
            if(pool != null)
            {
                pool.push(item);
            }
        }

        public void push(GameObject item)
        {
            PoolItem poolItem = item.GetComponent<PoolItem>();
            if(poolItem == null)
            {
                return;
            }
            ObjectPool pool = getPool(poolItem.poolId);
            if(pool != null)
            {
                pool.push(item);
            }
        }

        public IEnumerator preCache(int poolId, int count)
        {
            ObjectPool pool = getPool(poolId);
            if(pool == null)
            {
                yield break;
            }

            //print("Pre cache " + count + " items for pool " + poolId);

            for(int i = 0; i < count; i++)
            {
                yield return StartCoroutine(pools[i].preCacheItems(0, iterationsPerFrame, count));
            }
        }

        public IEnumerator clean()
        {
            //print("Clean ObjectPoolManager");

            int iterations = 0;
            int count = pools.Length;
            for(int i = 0; i < count; i++)
            {
                yield return StartCoroutine(pools[i].clean(iterations, iterationsPerFrame));
                iterations = pools[i].iterations;
            }
        }
    }
}