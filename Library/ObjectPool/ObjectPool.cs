﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace Vivat
{
    [System.Serializable]
    public class ObjectPool
    {
        public string poolName;
        [HideInInspector]
        public int poolId;
        public GameObject prefab;
        public int initCount;
        [HideInInspector]
        public GameObject container;
        public List<GameObject> cachedItems;
        public List<GameObject> activeItems;
        [HideInInspector]
        public int iterations = 0;

        public void init(Transform parent)
        {
            poolId = Animator.StringToHash(poolName);

            container = new GameObject("Pool-" + poolName);
            container.transform.parent = parent;
            cachedItems = new List<GameObject>();
            activeItems = new List<GameObject>();
        }

        public IEnumerator preCacheItems(int iterationsCurrent, int iterationsMax, int count = 0)
        {
            if(count == 0)
            {
                yield break;
            }

            //Debug.Log("Start init pool " + poolId + " iterations " + iterationsCurrent);

            iterations = iterationsCurrent;
            GameObject newObj;
            for(int i = 0; i < count; i++)
            {
                newObj = GameObject.Instantiate(prefab) as GameObject;
                push(newObj);

                iterations++;
                if(iterations == iterationsMax)
                {
                    //Debug.Log("Init pool iterations = " + iterationsMax + " wait frame");

                    iterations = 0;
                    yield return null;
                }
            }
        }

        public GameObject pop()
        {
            GameObject pooledObject;

            if(cachedItems.Count > 0)
            {
                int index = cachedItems.Count - 1;
                pooledObject = cachedItems[index];
                cachedItems.RemoveAt(index);
                pooledObject.transform.parent = null;
                pooledObject.SetActive(true);
            }
            else
            {
                pooledObject = GameObject.Instantiate(prefab) as GameObject;

                PoolItem poolItem = pooledObject.GetComponent<PoolItem>();
                poolItem.poolId = poolId;
            }

            activeItems.Add(pooledObject);

            return pooledObject;
        }

        public void push(GameObject newObj, bool cleanFromActive = true)
        {
            if(cleanFromActive)
            {
                activeItems.Remove(newObj);
            }

            newObj.transform.parent = container.transform;
            newObj.SetActive(false);

            cachedItems.Add(newObj);
        }

        public IEnumerator clean(int iterationsCurrent, int iterationsMax)
        {
            //Debug.Log("Start clean pool " + poolId + " iterations " + iterationsCurrent);

            int count = activeItems.Count;
            if(count == 0)
            {
                yield break;
                ;
            }

            iterations = iterationsCurrent;
            for(int i = 0; i < count; i++)
            {
                push(activeItems[i], false);

                iterations++;
                if(iterations == iterationsMax)
                {
                    //Debug.Log("Clean pool iterations = " + iterationsMax + " wait frame");

                    iterations = 0;
                    yield return null;
                }
            }

            activeItems.Clear();
        }
    }
}