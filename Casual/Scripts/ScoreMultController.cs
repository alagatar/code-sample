﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Vivat;

public class ScoreMultController:MonoBehaviour, NotifyListener
{
    public Text title;
    public Image bar;
    public int multiplier = 1;
    public int maxMultiplier = 10;
    public float currentProgress = 0;
    public float maxProgress = 10;
    public float step = 1.0f;
    public float timeout = 2.0f;
    public Animator animator;

    float counter = 0;
    bool working = false;

    static readonly int anim_show = Animator.StringToHash("show");

    void Start()
    {
        title.text = "x1";
        bar.fillAmount = 0f;

        NotificationCenter.addListener("increaseScore", this);
        NotificationCenter.addListener("changeLevelState", this);
    }

    void StartWork()
    {
        working = true;
        StartCoroutine("UpdateProgress");
    }

    void StopWork()
    {
        working = false;
        StopCoroutine("UpdateProgress");
    }

    IEnumerator UpdateProgress()
    {
        while(true)
        {
            yield return null;

            counter += Time.deltaTime;
            if(counter >= timeout)
            {
                counter = 0;
                decreaseProgress();
            }
        }
    }

    void decreaseProgress()
    {
        currentProgress -= step;
        if(currentProgress < 0)
        {
            if(multiplier == 1)
            {
                currentProgress = 0;
                StopWork();
            }
            else
            {
                multiplier--;
                currentProgress += maxProgress;
            }

            title.text = "x" + multiplier;
        }

        updateBar();
    }

    void updateBar()
    {
        float percent = currentProgress / maxProgress;
        bar.fillAmount = percent;
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(eventId == "increaseScore")
        {
            onIncreaseScore(args);
        }
        else if(eventId == "changeLevelState")
        {
            onChangeLevelState((string)args[0]);
        }
    }

    void onIncreaseScore(object[] args)
    {
        int value = (int)args[1];
        increaseProgress(value);
    }

    void increaseProgress(int value)
    {
        increaseCurrent(value);
        
        if(!working)
        {
            StartWork();
        }

        updateBar();
    }

    void increaseCurrent(int value)
    {
        currentProgress += value;
        if(currentProgress >= maxProgress)
        {
            increaseMultiplier();
        }
    }

    void increaseMultiplier()
    {
        int count = Mathf.FloorToInt(currentProgress / maxProgress);
        multiplier += count;
        currentProgress -= (maxProgress * count);

        if(multiplier > maxMultiplier)
        {
            multiplier = maxMultiplier;
            currentProgress = maxProgress - 1;
        }

        title.text = "x" + multiplier;

        animator.SetTrigger(anim_show);
    }

    void onChangeLevelState(string state)
    {
        if(state == "stop" || state == "pause")
        {
            onStopGame();
        }
        else if(state == "start" || state == "resume")
        {
            onStartGame();
        }
        else if(state == "reset")
        {
            onResetGame();
        }
    }

    void onStopGame()
    {
        if(working)
        {
            StopWork();
        }
    }

    void onStartGame()
    {
        StartWork();
    }

    void onResetGame()
    {
        multiplier = 1;
        currentProgress = 0;
        title.text = "x" + multiplier;
        updateBar();
    }
}