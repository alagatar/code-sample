﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivat;

public class TurnHintController:MonoBehaviour, NotifyListener
{
    public float timeoutInSeconds = 1.0f;
    public GameObject pointPrefab;
    public InputController inputController;
    public BoardController boardController;
    public TaskController taskController;
    public int maxCalcPerFrame = 30;
    public bool tutorialMode = false;

    int calculationCounter;
    bool working;
    bool taskFounded;
    List<GameObject> points;
    List<GameObject> pointsCache;
    List<Direction> tempTurns;
    List<Direction> hintTurns;
    Task[] taskList;
    int checkDepth;
    ShuffleBagCollection<int> randomCellGenerator;
    bool started = false;
    List<IntPoint> tutorialCells;

    void Start()
    {
        points = new List<GameObject>();
        pointsCache = new List<GameObject>();
        tempTurns = new List<Direction>();
        hintTurns = new List<Direction>();
        taskList = new Task[3];
        initGenerator();

        NotificationCenter.addListener("endTurn", this);
        NotificationCenter.addListener("addTurn", this);
        NotificationCenter.addListener("changeLevelState", this);

        startWork();
    }

    void initGenerator()
    {
        randomCellGenerator = new ShuffleBagCollection<int>();
        int countCells = boardController.cols * boardController.rows;
        for(int i = 0; i < countCells; i++)
        {
            randomCellGenerator.add(i);
        }
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(eventId == "endTurn")
        {
            startWork();
        }
        else if(eventId == "addTurn")
        {
            stopWork();
        }
        else if(eventId == "changeLevelState")
        {
            onChangeLevelState((string)args[0]);
        }
    }

    void startWork()
    {
        if(tutorialMode)
        {
            return;
        }

        if(working)
        {
            return;
        }
        working = true;

        taskFounded = false;
        calculationCounter = 0;
        started = false;

        StopAllCoroutines();
        StartCoroutine("waitTimeout");
    }

    void stopWork()
    {
        if(!working)
        {
            return;
        }
        working = false;
        started = false;

        StopAllCoroutines();
        hidePoints();
    }

    IEnumerator waitTimeout()
    {
        yield return new WaitForSeconds(timeoutInSeconds);
        yield return StartCoroutine("seekHint"); 
    }

    IEnumerator seekHint()
    {
        started = true;

        initTaskList();

        int minDepth = boardController.minDepth;
        int maxDepth = boardController.maxDepth;

        for(int i = minDepth; i <= maxDepth; i++)
        {
            yield return StartCoroutine("checkLayerForHint", i);
            if(taskFounded)
            {
                break;
            }

            yield return null;
        }

        started = false;
    }

    void initTaskList()
    {
        for(int i = 0; i < 3; i++)
        {
            taskList[i] = taskController.taskList[i];
        }

        Array.Sort(taskList, CompareTasksByTurns);
    }

    int CompareTasksByTurns(Task a, Task b)
    {
        return a.turns.Length - b.turns.Length;
    }

    IEnumerator checkLayerForHint(int depth)
    {
        checkDepth = depth;

        for(int i = 0; i < 3; i++)
        {
            yield return StartCoroutine("checkCanCompleteTask", i);
            if(taskFounded)
            {
                break;
            }
        }
    }

    IEnumerator checkCanCompleteTask(int taskIndex)
    {
        Task targetTask = taskList[taskIndex];
        if(targetTask == null)
        {
            yield break;
        }

        int cols = boardController.cols;
        int rows = boardController.rows;
        Piece startPiece;

        randomCellGenerator.reset();

        int count = cols * rows;
        int cellIndex;
        for(int i = 0; i < count; i++)
        {
            cellIndex = randomCellGenerator.next();
            IntPoint cell = boardController.getPositionByIndex(cellIndex);

            startPiece = boardController.getPieceOnPosition(cell.x, cell.y);
            if(startPiece == null || 
                startPiece.depth != checkDepth)
            {
                continue;
            }

            yield return StartCoroutine(checkCanCompleteTaskOn(cell.x, cell.y, targetTask));
            if(taskFounded)
            {
                yield break;
            }
        }
    }

    IEnumerator checkCanCompleteTaskOn(int col, int row, Task task)
    {
        Piece startPiece = boardController.getPieceOnPosition(col, row);
        if(startPiece == null)
        {
            yield break;
        }

        int depth = startPiece.depth;

        fillTempTurns(task);
        if(checkMatchToTaskWithRotations(col, row, depth))
        {
            taskFounded = true;
            yield break;
        }

        if(calculationCounter >= maxCalcPerFrame)
        {
            calculationCounter = 0;
            yield return null;
        }

        fillTempTurns(task);
        invertDirectionsBackward();

        if(checkMatchToTaskWithRotations(col, row, depth))
        {
            taskFounded = true;
            yield break;
        }

        if(calculationCounter >= maxCalcPerFrame)
        {
            calculationCounter = 0;
            yield return null;
        }

        if(task.allowInverse)
        {
            fillTempTurns(task);
            invertDirectionsHorizontal();

            if(checkMatchToTaskWithRotations(col, row, depth))
            {
                taskFounded = true;
                yield break;
            }

            if(calculationCounter >= maxCalcPerFrame)
            {
                calculationCounter = 0;
                yield return null;
            }

            invertDirectionsHorizontalAndBackward();

            if(checkMatchToTaskWithRotations(col, row, depth))
            {
                taskFounded = true;
                yield break;
            }
        }

        yield return null;
    }

    void fillTempTurns(Task task)
    {
        tempTurns.Clear();
        tempTurns.AddRange(task.turns);
    }

    bool checkMatchToTaskWithRotations(int col, int row, int depth)
    {
        for(int i = 0; i < 4; i++)
        {
            if(i > 0)
            {
                rotateTempTurns();
            }

            if(checkMatchToTaskRotate(col, row, depth))
            {
                showHintAt(col, row);
                return true;
            }
        }
        return false;
    }

    void rotateTempTurns()
    {
        int count = tempTurns.Count;
        for(int i = 0; i < count; i++)
        {
            tempTurns[i] = DirectionTools.rotateDirection(tempTurns[i]);
        }
    }

    bool checkMatchToTaskRotate(int col, int row, int depth)
    {
        calculationCounter++;

        int checkCol = col;
        int checkRow = row;
        int countSteps = tempTurns.Count;
        IntPoint change;
        Piece checkPiece;
        for(int i = 1; i < countSteps; i++)
        {
            change = DirectionTools.getChangeByDirection(tempTurns[i]);
            checkCol += change.x;
            checkRow += change.y;

            checkPiece = boardController.getPieceOnPosition(checkCol, checkRow);
            if(checkPiece == null || checkPiece.depth != depth)
            {
                return false;
            }
        }
        return true;
    }

    void invertDirectionsBackward()
    {
        int count = tempTurns.Count;
        tempTurns.Reverse(1, count - 1);

        for(int i = 1; i < count; i++)
        {
            tempTurns[i] = DirectionTools.invertDirection(tempTurns[i]);
        }
    }

    void invertDirectionsHorizontal()
    {
        int count = tempTurns.Count;
        for(int i = 0; i < count; i++)
        {
            tempTurns[i] = DirectionTools.invertDirectionHorizontal(tempTurns[i]);
        }
    }

    void invertDirectionsHorizontalAndBackward()
    {
        int count = tempTurns.Count;
        tempTurns.Reverse(1, count - 1);

        for(int i = 1; i < count; i++)
        {
            tempTurns[i] = DirectionTools.invertDirectionHorizontal(tempTurns[i]);
            tempTurns[i] = DirectionTools.invertDirection(tempTurns[i]);
        }
    }

    void showHintAt(int col, int row)
    {
        hintTurns.AddRange(tempTurns);

        createPoint(col, row);

        int checkCol = col;
        int checkRow = row;
        int countSteps = hintTurns.Count;
        IntPoint change;
        for(int i = 1; i < countSteps; i++)
        {
            change = DirectionTools.getChangeByDirection(tempTurns[i]);
            checkCol += change.x;
            checkRow += change.y;

            createPoint(checkCol, checkRow);
        }

        hintTurns.Clear();
    }

    void hidePoints()
    {
        if(points == null)
        {
            return;
        }

        int count = points.Count;
        if(count == 0)
        {
            return;
        }

        for(int i = 0; i < count; i++)
        {
            delPoint(i, false);
        }

        points.Clear();
    }

    void delPoint(int index, bool clear = true)
    {
        GameObject point = points[index];
        if(clear)
        {
            points.RemoveAt(index);
        }
        point.SetActive(false);
        pointsCache.Add(point);
    }

    void createPoint(int col, int row)
    {
        GameObject point = getPoint();
        points.Add(point);

        FloatPoint position = boardController.getWorldPosition(col, row);
        point.transform.position = new Vector3(position.x, position.y, 0);
    }

    GameObject getPoint()
    {
        GameObject newItem;
        if(pointsCache.Count > 0)
        {
            int count = pointsCache.Count;
            newItem = pointsCache[count - 1];
            pointsCache.RemoveAt(count - 1);
        }
        else
        {
            newItem = Instantiate(pointPrefab, new Vector3(), Quaternion.identity) as GameObject;
        }
        newItem.transform.parent = transform;
        newItem.SetActive(true);
        return newItem;
    }

    void onChangeLevelState(string state)
    {
        if(state == "stop")
        {
            stopWork();
        }
        else if(state == "start" || state == "reset")
        {
            startWork();
        }
        else if(state == "pause")
        {
            onPause();
        }
        else if(state == "resume")
        {
            onResume();
        }
    }

    void onPause()
    {
        if(!working)
        {
            return;
        }
        working = false;

        StopAllCoroutines();
    }

    void onResume()
    {
        if(tutorialMode)
        {
            return;
        }

        if(working)
        {
            return;
        }
        working = true;

        taskFounded = false;
        calculationCounter = 0;
        
        StopAllCoroutines();
        
        if(started)
        {
            StartCoroutine("seekHint");
        }
    }

    public void startTutorial()
    {
        tutorialMode = true;
        tutorialCells = new List<IntPoint>();
    }

    public void completeTutorial()
    {
        tutorialMode = false;
        hidePoints();
        startWork();
    }

    public void setTutorialCells(List<IntPoint> cells)
    {
        tutorialCells.Clear();
        tutorialCells.AddRange(cells);

        StartCoroutine("showTutorialCells");
    }

    IEnumerator showTutorialCells()
    {
        yield return null;

        hidePoints();
        int count = tutorialCells.Count;
        for(int i = 0; i < count; i++)
        {
            createPoint(tutorialCells[i].x, tutorialCells[i].y);
        }
    }
}