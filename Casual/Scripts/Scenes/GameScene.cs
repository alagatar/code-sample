using System.Collections;
using UnityEngine;
using Vivat;

public class GameScene:MonoBehaviour, NotifyListener
{
    enum GameState {None, Play, Stop, Pause};

    public RankPanel rankPanel;
    public ResultMenu resultMenu;
    public PauseMenu pauseMenu;
    public NoAdsMenu noAdsMenu;
    public RankMenu rankMenu;
    public float rankDelay = 0.3f;

    GameState state;

    void Start()
    {
        state = GameState.Play;

        NotificationCenter.addListener("endTurns", this);
        NotificationCenter.addListener("changeGameState", this);
        NotificationCenter.addListener("showMenu", this);
        NotificationCenter.addListener("nextRank", this);

        TapdaqController.instance.triggerInterstitialAds();
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(eventId == "endTurns" && state == GameState.Play)
        {
            onEndTurns();
        }
        else if(eventId == "changeGameState")
        {
            onChangeGameState((string)args[0]);
        }
        else if(eventId == "showMenu")
        {
            onShowMenuEvent(args);
        }
        else if(eventId == "nextRank")
        {
            onNextRank();
        }
    }

    void onEndTurns()
    {
        Job newJob = new Job();
        newJob.callback = finishGame;
        
        NotificationCenter.sendNotify("jobEvent", "new", newJob);
    }

    void finishGame()
    {
        state = GameState.Stop;
        saveData();
        resultMenu.show();
        stopGame();
        
        NotificationCenter.sendNotify("jobEvent", "complete");
    }

    void saveData()
    {
        int completedPercent = rankPanel.getCompletePercent();
        PlayerPrefs.SetInt("lastProgress", completedPercent);
        int currentPercent = PlayerPrefs.GetInt("bestProgress", 0);
        if(completedPercent > currentPercent)
        {
            PlayerPrefs.SetInt("bestProgress", completedPercent);
        }
    }

    void stopGame()
    {
        NotificationCenter.sendNotify("changeLevelState", "stop");
    }

    void onChangeGameState(string state)
    {
        if(state == "quit")
        {
            quitGame();
        }
        else if(state == "restart")
        {
            restartGame();
        }
        else if(state == "resume")
        {
            resumeGame();
        }
        else if(state == "revive")
        {
            restartGame(false);
        }
    }

    void OnDestroy()
    {
        NotificationCenter.clear();
    }

    public void quitGame()
    {
        SceneLoadManager.instance.loadScene("Menu");
    }

    public void restartGame(bool reset = true)
    {
        if(resultMenu.active)
        {
            resultMenu.hide();
        }
        else if(pauseMenu.active)
        {
            pauseMenu.hide();
        }
        
        NotificationCenter.sendNotify("resetGrid");
        NotificationCenter.sendNotify("changeLevelState", "reset", reset);

        state = GameState.Play;

        TapdaqController.instance.triggerInterstitialAds();
    }
    
    public void pauseGame()
    {
        NotificationCenter.sendNotify("changeLevelState", "pause");

        pauseMenu.show();
    }

    public void resumeGame()
    {
        pauseMenu.hide();
        
        NotificationCenter.sendNotify("changeLevelState", "resume");
    }

    void onShowMenuEvent(object[] args)
    {
        string menuId = (string)args[0];
        if(menuId == "noAds")
        {
            noAdsMenu.show();
        }
    }

    void onNextRank()
    {
        StartCoroutine("waitBeforeNextRank");
    }

    IEnumerator waitBeforeNextRank()
    {
        yield return new WaitForSeconds(rankDelay);

        int rank = Main.instance.rank;
        rankMenu.targetRank = rank;

        Job newJob = new Job();
        newJob.callback = rankMenu.show;
        
        NotificationCenter.sendNotify("jobEvent", "new", newJob);

        AchievementManager.instance.trigger("st_level");
    }
}