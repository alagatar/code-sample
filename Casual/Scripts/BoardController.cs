﻿using System.Collections.Generic;
using UnityEngine;
using Vivat;
using DG.Tweening;

public class BoardController:MonoBehaviour, NotifyListener
{
    public int cols;
    public int rows;
    public GameObject prefab;
    public float cellSpaceX;
    public float cellSpaceY;
    public float cellWidth;
    public float cellHeight;
    public BlockSkins blockSkins;
    public int minDepth = 0;
    public int maxDepth = 0;
    public int skin;
    public BoardConfig config;

    float halfCellSpaceX;
    float halfCellSpaceY;
    Piece[,] pieces;
    List<Piece> piecesCache;
    List<BoardBlock> boardBuildConfig;

    void Start()
    {
        NotificationCenter.addListener("resetGridHead", this);
        NotificationCenter.addListener("resetGrid", this);
        NotificationCenter.addListener("nextRank", this);
        NotificationCenter.addListener("cachePiece", this);

        halfCellSpaceX = cellSpaceX / 2.0f;
        halfCellSpaceY = cellSpaceY / 2.0f;

        piecesCache = new List<Piece>();
        skin = Main.instance.skin;
        boardBuildConfig = new List<BoardBlock>();

        createGrid();
    }

    void createGrid()
    {
        pieces = new Piece[rows, cols];

        initStartDepth();
        initRankConfig();
        
        int count = boardBuildConfig.Count;
        BoardBlock block;
        for(int i = 0; i < count; i++)
        {
            block = boardBuildConfig[i];
            createItem(block.x, block.y, minDepth);
        }

        //for(int row = 0; row < rows; row++)
        //{
        //    for(int col = 0; col < cols; col++)
        //    {
        //        if(checkPresentConfig(col, row))
        //        {
        //            createItem(col, row, minDepth);
        //        }
        //    }
        //}
    }

    void initStartDepth()
    {
        int prevDepth = PlayerPrefs.GetInt("prevStartDepth", -1);
        minDepth = Tools.randomIntExclude(0, blockSkins.blocks.Length, prevDepth);
        maxDepth = minDepth;
        PlayerPrefs.SetInt("prevStartDepth", minDepth);
    }

    void initRankConfig()
    {
        int rank = Main.instance.rank;
        BoardRanksConfig rankConfig;
        for(int i = 1; i <= rank; i++)
        {
            rankConfig = config.getConfigForRank(i);
            if(rankConfig != null)
            {
                addConfigFrom(rankConfig.blocks);
            }
        }
    }

    void addConfigFrom(BoardBlock[] blockConfig)
    {
        int count = blockConfig.Length;
        BoardBlock block;
        for(int i = 0; i < count; i++)
        {
            block = blockConfig[i];
            //boardBuildConfig.Add(block);

            if(!block.blocked)
            {
                boardBuildConfig.Add(block);
            }
            else
            {
                delBlockOn(block.x, block.y);
            }
        }
    }

    void delBlockOn(int x, int y)
    {
        int count = boardBuildConfig.Count;
        for(int i = 0; i < count; i++)
        {
            if(boardBuildConfig[i].x == x && 
                boardBuildConfig[i].y == y)
            {
                boardBuildConfig.RemoveAt(i);
                break;
            }
        }
        destroyCell(x, y);
    }

    void destroyCell(int x, int y)
    {
        Piece piece = pieces[x, y];
        if(piece == null)
        {
            return;
        }

        GameObject view = piece.view;
        view.SetActive(false);
        pieces[x, y] = null;

        piecesCache.Add(piece);
    }

    bool checkPresentConfig(int col, int row)
    {
        int count = boardBuildConfig.Count;
        for(int i = 0; i < count; i++)
        {
            if(boardBuildConfig[i].x == col && 
                boardBuildConfig[i].y == row)
            {
                return true;
            }
        }
        return false;
    }

    void createItem(int col, int row, int depth)
    {
        var cellPos = getWorldPosition(col, row);
        var piece = createNewPiece(depth, cellPos);
        piece.x = col;
        piece.y = row;
        piece.depth = depth;
        pieces[col, row] = piece;

        piece.show();
    }

    Piece createNewPiece(int depth, FloatPoint cellPos)
    {
        Piece newPiece;
        GameObject view;
        if(piecesCache.Count > 0)
        {
            int index = piecesCache.Count - 1;
            newPiece = piecesCache[index];
            piecesCache.RemoveAt(index);
            view = newPiece.view;
            view.SetActive(true);
        }
        else
        {
            view = Instantiate(prefab, new Vector3(), Quaternion.identity) as GameObject;
            newPiece = view.GetComponent<Piece>();
            newPiece.view = view;
        }

        setGraphForPiece(newPiece, depth, cellPos);

        return newPiece;
    }

    void setGraphForPiece(Piece newPiece, int depth, FloatPoint cellPos)
    {
        int spriteId = Mathf.FloorToInt(depth % blockSkins.blocks.Length);
        newPiece.bodyRender.sprite = blockSkins.getSkin(spriteId, skin);

        int graphIndex = depth * 2;
        newPiece.view.transform.position = new Vector3(cellPos.x, cellPos.y, graphIndex);
        newPiece.view.transform.parent = transform;
    }

    public IntPoint getGridPosition(float x, float y)
    {
        var position = new IntPoint();
        position.x = Mathf.FloorToInt((x - transform.position.x) / cellWidth);
        position.y = Mathf.FloorToInt((y - transform.position.y) / cellHeight);
        return position;
    }

    public FloatPoint getWorldPosition(int x, int y)
    {
        FloatPoint position = new FloatPoint();
        position.x = transform.position.x + halfCellSpaceX + (x * cellSpaceX);
        position.y = transform.position.y + halfCellSpaceY + (y * cellSpaceY);
        return position;
    }

    public bool checkCellInGrid(int x, int y)
    {
        if(x < 0 || x >= cols ||
            y < 0 || y >= rows)
        {
            return false;
        }
        return true;
    }

    public Piece getPieceOnPosition(int x, int y)
    {
        if(!checkCellInGrid(x, y))
        {
            return null;
        }
        return pieces[x, y];
    }

    public void clearCellOnPosition(int x, int y, bool updateTopDepth = true)
    {
        Piece piece = pieces[x, y];
        if(piece == null)
        {
            return;
        }

        var pieceDepth = piece.depth;
        pieceDepth++;

        animateDestroyCell(x, y);
        createItem(x, y, pieceDepth);

        if(updateTopDepth)
        {
            if(pieceDepth > maxDepth)
            {
                maxDepth = pieceDepth;
            }
        }
    }

    void animateDestroyCell(int x, int y)
    {
        Piece piece = pieces[x, y];
        if(piece == null)
        {
            return;
        }

        piece.jump();
        pieces[x, y] = null;
    }

    void jumpPiece(Piece target)
    {
        Vector3 jumpPositionPosition = calcJumpPosition(target);

        float jumpPower = 2f;
        float jumpDuration = 0.5f;
        float delay = UnityEngine.Random.Range(0f, 0.5f);

        Vector3 pos = target.view.transform.position;
        pos.z = -1;
        target.view.transform.position = pos;

        target.view.transform.DOJump(jumpPositionPosition, jumpPower, 1, jumpDuration).SetDelay(delay);
    }

    Vector3 calcJumpPosition(Piece target)
    {
        Vector3 targetPos = target.view.transform.position;

        int side = 1;
        if(UnityEngine.Random.Range(1, 10) > 5)
        {
            side = -1;
        }
        float dist = UnityEngine.Random.Range(0.5f, 2f) * side;

        targetPos.x += dist;
        targetPos.y -= UnityEngine.Random.Range(1f, 3f);
        return targetPos;
    }

    public void checkDepth()
    {
        while(true)
        {
            if(checkPresentPieceWithDepth(minDepth))
            {
                break;
            }
            minDepth++;
        }
    }

    bool checkPresentPieceWithDepth(int depth)
    {
        for(int row = 0; row < rows; row++)
        {
            for(int col = 0; col < cols; col++)
            {                
                var piece = pieces[col, row];
                if(piece == null)
                {
                    continue;
                }

                if(piece.depth == minDepth)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(eventId == "resetGridHead")
        {
            clearGridTopLayer();
        }
        else if(eventId == "resetGrid")
        {
            resetGrid();
        }
        else if(eventId == "nextRank")
        {
            onUpdateRank((int)args[0]);
        }
        else if(eventId == "cachePiece")
        {
            cachePiece((Piece)args[0]);
        }
    }

    void clearGridTopLayer()
    {
        int countCells = 0;
        for(int row = 0; row < rows; row++)
        {
            for(int col = 0; col < cols; col++)
            {
                var piece = pieces[col, row];
                if(piece == null)
                {
                    continue;
                }

                if(piece.depth == minDepth)
                {
                    countCells++;
                    clearCellOnPosition(col, row, false);
                }
            }
        }

        checkDepth();
    }

    void resetGrid()
    {
        initStartDepth();

        for(int row = 0; row < rows; row++)
        {
            for(int col = 0; col < cols; col++)
            {
                resetItem(col, row, minDepth);
            }
        }
    }

    void resetItem(int col, int row, int topDepth)
    {
        Piece piece = pieces[col, row];
        if(piece == null)
        {
            return;
        }
        piece.depth = topDepth;
        var cellPos = getWorldPosition(col, row);
        setGraphForPiece(piece, topDepth, cellPos);
    }

    public IntPoint getPositionByIndex(int index)
    {
        var position = new IntPoint();
        position.x = index % cols;
        position.y = (int)(index / cols);
        return position;
    }

    void onUpdateRank(int rank)
    {
        BoardRanksConfig rankConfig = config.getConfigForRank(rank);
        if(rankConfig != null)
        {
            updateBoardWithConfig(rankConfig);
        }
    }

    void updateBoardWithConfig(BoardRanksConfig rankConfig)
    {
        boardBuildConfig.Clear();
        addConfigFrom(rankConfig.blocks);

        int count = boardBuildConfig.Count;
        for(int i = 0; i < count; i++)
        {
            createItem(boardBuildConfig[i].x, boardBuildConfig[i].y, minDepth);
        }
    }

    void cachePiece(Piece piece)
    {
        piecesCache.Add(piece);
    }
}