using UnityEngine;
using Vivat;

public class PauseMenu:BaseMenu, NotifyListener
{
    public Animator animator;
    public GameObject noAdsButton;

    static readonly int anim_show = Animator.StringToHash("show");
    static readonly int anim_hide = Animator.StringToHash("hide");

    override public void show()
    {
        base.show();

        animator.SetTrigger(anim_show);
    }

    override public void init()
    {
        if(TapdaqController.instance.adsEnabled)
        {
            NotificationCenter.addListener("removeAds", this);
        }
        else
        {
            noAdsButton.SetActive(false);
        }
    }

    override public void hide()
    {
        base.hide();

        animator.SetTrigger(anim_hide);
    }

    public void onQuit()
    {
        NotificationCenter.sendNotify("changeGameState", "quit");
    }

    public void onRemoveAds()
    {
        NotificationCenter.sendNotify("buyRemoveAds");
    }

    public void onRestart()
    {
        NotificationCenter.sendNotify("changeGameState", "restart");
    }

    public void onContinue()
    {
        NotificationCenter.sendNotify("changeGameState", "resume");
    }

    public void onNotify(string eventId, params object[] args)
    {
        completeRemoveAds();
    }

    void completeRemoveAds()
    {
        noAdsButton.SetActive(false);
    }
}