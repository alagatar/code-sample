﻿using UnityEngine;
using Vivat;

public class NoAdsMenu:BaseMenu
{
    public Animator animator;

    static readonly int anim_show = Animator.StringToHash("show");
    static readonly int anim_hide = Animator.StringToHash("hide");

    override public void show()
    {
        base.show();

        animator.SetTrigger(anim_show);
    }

    override public void hide()
    {
        base.hide();

        animator.SetTrigger(anim_hide);
    }

    public void onContinue()
    {
        hide();
        
        NotificationCenter.sendNotify("closeMenu", "noAds");
    }
}