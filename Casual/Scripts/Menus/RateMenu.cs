﻿using UnityEngine;
using Vivat;

public class RateMenu:BaseMenu
{
    public Animator animator;
    public GameObject part1Container;
    public GameObject part2Container;

    static readonly int anim_show = Animator.StringToHash("show");
    static readonly int anim_hide = Animator.StringToHash("hide");
    int step = 1;

    override public void show()
    {
        base.show();

        animator.SetTrigger(anim_show);
    }

    override public void hide()
    {
        base.hide();

        animator.SetTrigger(anim_hide);
    }

    public void noHandler()
    {
        hide();

        NotificationCenter.sendNotify("closeMenu", "rate");
    }

    public void yesHandler()
    {
        if(step == 1)
        {
            showStep2();
        }
        else
        {
            Main.instance.rateIt();
            hide();
        }
    }

    void showStep2()
    {
        step = 2;
        part1Container.SetActive(false);
        part2Container.SetActive(true);
    }
}