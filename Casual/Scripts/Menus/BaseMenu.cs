﻿using System.Collections;
using UnityEngine;

public class BaseMenu:MonoBehaviour
{
    [HideInInspector]
    public bool active = false;
    public float disableTimeout = 0.5f;

    bool inited = false;

    public virtual void show()
    {
        active = true;
        StopCoroutine("waitAndDisable");

        if(!inited)
        {
            inited = true;
            init();
        }

        gameObject.SetActive(true);
    }

    public virtual void init()
    {
    }

    public virtual void hide()
    {
        active = false;

        StopCoroutine("waitAndDisable");
        StartCoroutine("waitAndDisable");
    }

    IEnumerator waitAndDisable()
    {
        yield return new WaitForSeconds(disableTimeout);
        gameObject.SetActive(false);
    }
}