﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivat;

public class EndTurnsController:MonoBehaviour, NotifyListener
{
    public BoardController boardController;
    public TaskController taskController;
    public int maxCalcPerFrame = 30;
    public bool tutorialMode = false;

    List<Direction> tempTurns;
    int calculationCounter;
    bool taskFounded;
    bool started = false;

    void Start()
    {
        NotificationCenter.addListener("completeTask", this);
        NotificationCenter.addListener("changeLevelState", this);
        tempTurns = new List<Direction>();
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(eventId == "completeTask")
        {
            onCompleteTask();
        }
        else if(eventId == "changeLevelState")
        {
            onChangeLevelState(args);
        }
    }

    void onCompleteTask()
    {
        StopAllCoroutines();
        startWork();
    }
    
    IEnumerator checkEndTurns()
    {
        calculationCounter = 0;
        taskFounded = false;
        started = true;

        int count = taskController.maxTaskCount;
        for(int i = 0; i < count; i++)
        {
            yield return StartCoroutine(checkCanCompleteTask(i));
            if(taskFounded)
            {
                started = false;
                yield break;
            }
        }

        started = false;
        noTaskCanComplete();
    }

    #region TaskCompleteAlgoritm

    IEnumerator checkCanCompleteTask(int taskIndex)
    {
        Task targetTask = taskController.taskList[taskIndex];
        if(targetTask == null)
        {
            yield break;
        }
        
        int rows = boardController.rows;
        int cols = boardController.cols;
        for(int row = 0; row < rows; row++)
        {
            for(int col = 0; col < cols; col++)
            {
                yield return StartCoroutine(checkCanCompleteTaskOn(col, row, targetTask));
                if(taskFounded)
                {
                    yield break;
                }
            }
        }
    }

    IEnumerator checkCanCompleteTaskOn(int col, int row, Task task)
    {
        Piece startPiece = boardController.getPieceOnPosition(col, row);
        if(startPiece == null)
        {
            yield break;
        }

        int depth = startPiece.depth;

        fillTempTurns(task);
        if(checkMatchToTaskWithRotations(col, row, depth))
        {
            taskFounded = true;
            yield break;
        }

        if(calculationCounter >= maxCalcPerFrame)
        {
            calculationCounter = 0;
            yield return null;
        }

        fillTempTurns(task);
        invertDirectionsBackward();

        if(checkMatchToTaskWithRotations(col, row, depth))
        {
            taskFounded = true;
            yield break;
        }

        if(calculationCounter >= maxCalcPerFrame)
        {
            calculationCounter = 0;
            yield return null;
        }

        if(task.allowInverse)
        {
            fillTempTurns(task);
            invertDirectionsHorizontal();

            if(checkMatchToTaskWithRotations(col, row, depth))
            {
                taskFounded = true;
                yield break;
            }

            if(calculationCounter >= maxCalcPerFrame)
            {
                calculationCounter = 0;
                yield return null;
            }

            invertDirectionsHorizontalAndBackward();

            if(checkMatchToTaskWithRotations(col, row, depth))
            {
                taskFounded = true;
                yield break;
            }
        }
    }

    void fillTempTurns(Task task)
    {
        tempTurns.Clear();
        tempTurns.AddRange(task.turns);
    }

    bool checkMatchToTaskWithRotations(int col, int row, int depth)
    {
        for(int i = 0; i < 4; i++)
        {
            if(i > 0)
            {
                rotateTempTurns();
            }

            if(checkMatchToTaskRotate(col, row, depth))
            {
                return true;
            }
        }
        return false;
    }

    bool checkMatchToTaskRotate(int col, int row, int depth)
    {
        calculationCounter++;

        int checkCol = col;
        int checkRow = row;
        int countSteps = tempTurns.Count;
        IntPoint change;
        Piece checkPiece;
        for(int i = 1; i < countSteps; i++)
        {
            change = DirectionTools.getChangeByDirection(tempTurns[i]);
            checkCol += change.x;
            checkRow += change.y;

            checkPiece = boardController.getPieceOnPosition(checkCol, checkRow);
            if(checkPiece == null || checkPiece.depth != depth)
            {
                return false;
            }
        }
        return true;
    }

    void rotateTempTurns()
    {
        int count = tempTurns.Count;
        for(int i = 0; i < count; i++)
        {
            tempTurns[i] = DirectionTools.rotateDirection(tempTurns[i]);
        }
    }

    void invertDirectionsBackward()
    {
        int count = tempTurns.Count;
        tempTurns.Reverse(1, count - 1);
        
        for(int i = 1; i < count; i++)
        {
            tempTurns[i] = DirectionTools.invertDirection(tempTurns[i]);
        }
    }

    void invertDirectionsHorizontal()
    {
        int count = tempTurns.Count;
        for(int i = 0; i < count; i++)
        {
            tempTurns[i] = DirectionTools.invertDirectionHorizontal(tempTurns[i]);
        }
    }

    void invertDirectionsHorizontalAndBackward()
    {
        int count = tempTurns.Count;
        tempTurns.Reverse(1, count - 1);

        for(int i = 1; i < count; i++)
        {
            tempTurns[i] = DirectionTools.invertDirectionHorizontal(tempTurns[i]);
            tempTurns[i] = DirectionTools.invertDirection(tempTurns[i]);
        }
    }
    
    #endregion

    void noTaskCanComplete()
    {
        NotificationCenter.sendNotify("endTurns");
    }

    void onChangeLevelState(object[] args)
    {
        string state = (string)args[0];

        if(state == "stop")
        {
            onGameStop();
        }
        else if(state == "reset")
        {
            onGameReset();
        }
        else if(state == "pause")
        {
            onPause();
        }
        else if(state == "resume")
        {
            onResume();
        }
    }

    void onGameStop()
    {
        StopAllCoroutines();
    }

    void onGameReset()
    {
        startWork();
    }

    void onPause()
    {
        StopAllCoroutines();
    }

    void onResume()
    {
        if(!started)
        {
            return;
        }
        startWork();
    }

    void startWork()
    {
        if(tutorialMode)
        {
            return;
        }
        StartCoroutine("checkEndTurns");
    }

    public void completeTutorial()
    {
        tutorialMode = false;
        startWork();
    }
}