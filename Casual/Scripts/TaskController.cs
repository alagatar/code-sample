﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vivat;

public class TaskController:MonoBehaviour, NotifyListener
{
    public int maxTaskCount = 3;
    public Transform container;
    public Sprite[] sprites;
    public GameObject prefab;
    public Task[] taskList;
    public TasksConfig config;
    public bool tutorialMode = false;

    List<GameObject> taskViewCache;
    ShuffleBagCollection<int> generator;
    int tutorialStep = 0;

    void Start()
    {
        taskViewCache = new List<GameObject>();
                
        generator = new ShuffleBagCollection<int>();
        taskList = new Task[maxTaskCount];

        initTasks();

        if(tutorialMode)
        {
            createTask(0);
        }
        else
        {
            createTasks();
        }
                
        NotificationCenter.addListener("nextRank", this);
    }

    void initTasks()
    {
        int rank = Main.instance.rank;

        int countRanks = config.ranks.Length;
        if(rank > countRanks)
        {
            rank = countRanks;
        }

        for(int i = 1; i <= rank; i++)
        {
            addTasksFromConfig(i);
        }
        generator.reset();
    }

    void addTasksFromConfig(int rank)
    {
        RankConfig rankConfig = config.getConfigForRank(rank);
        if(rankConfig == null)
        {
            return;
        }

        int count = rankConfig.items.Length;
        for(int i = 0; i < count; i++)
        {
            generator.add(rankConfig.items[i], 1, false);
        }
    }

    void createTasks()
    {
        for(int i = 0; i < maxTaskCount; i++)
        {
            createTask(i);
        }
    }

    void createTask(int index)
    {
        if(tutorialMode)
        {
            createTutorialTasks(index);
            return;
        }

        int taskId = getNextTaskId();
        createTaskById(index, taskId);
    }

    void createTaskById(int index, int taskId)
    {
        var currentConfig = config.items[taskId];

        if(taskList[index] == null)
        {
            taskList[index] = new Task();
        }

        var task = taskList[index];

        task.id = taskId;
        task.allowInverse = currentConfig.allowInverse;
        int turnsCount = currentConfig.turns.Length;
        task.turns = new Direction[turnsCount];
        for(int i = 0; i < turnsCount; i++)
        {
            task.turns[i] = currentConfig.turns[i];
        }

        var taskView = getViewForTask(taskId);
        taskView.transform.SetAsLastSibling();
        task.view = taskView;
    }

    int getNextTaskId()
    {
        int taskId = generator.next();
        return taskId;
    }

    public void completeTask(int id)
    {
        SoundManager.instance.playSfx("collect");

        int taskIndex = getTaskIndex(id);
        if(taskIndex == -1)
        {
            return;
        }

        var task = taskList[taskIndex];
        int countSteps = task.turns.Length;

        hideTask(task);
        createTask(taskIndex);

        NotificationCenter.sendNotify("completeTask");
        NotificationCenter.sendNotify("clearBlocks", countSteps);

        if(id == 4 || id == 7 || id == 10 || id == 13 || id == 14)
        {
            AchievementManager.instance.trigger(string.Format("st_form{0}", id));
        }
    }

    void hideTask(Task task)
    {
        GameObject view = task.view;
        view.SetActive(false);
        taskViewCache.Add(view);
        task.view = null;
    }

    GameObject getViewForTask(int taskId)
    {
        GameObject view;
        if(taskViewCache.Count > 0)
        {
            int index = taskViewCache.Count - 1;
            view = taskViewCache[index];
            taskViewCache.RemoveAt(index);
            view.SetActive(true);
        }
        else
        {
            view = Instantiate(prefab, new Vector3(), Quaternion.identity) as GameObject;
        }
                
        view.transform.SetParent(container, false);
        view.GetComponent<Image>().sprite = sprites[taskId];
        return view;
    }

    int getTaskIndex(int id)
    {
        for(int i = 0; i < maxTaskCount; i++)
        {
            if(taskList[i] != null && taskList[i].id == id)
            {
                return i;
            }
        }
        return -1;
    }

    public void onNotify(string eventId, params object[] args)
    {
        onNewRank((int)args[0]);
    }

    void onNewRank(int rank)
    {
        if(config.checkPresentConfig(rank))
        {
            updateTasksDifficulty(rank);
        }
    }

    void updateTasksDifficulty(int rank)
    {
        addTasksFromConfig(rank);
        generator.reset();
    }

    void createTutorialTasks(int index)
    {
        tutorialStep++;
        if(tutorialStep == 4)
        {
            completeTutorial();
            return;
        }

        switch(tutorialStep)
        {
            case 1:
                createTaskById(0, 1);
                break;

            case 2:
                createTaskById(0, 2);
                break;

            case 3:
                createTaskById(0, 3);
                break;
        }
    }

    void completeTutorial()
    {
        tutorialMode = false;
        StartCoroutine("showTaskAfterTutorial");
    }

    IEnumerator showTaskAfterTutorial()
    {
        for(int i = 0; i < 3; i++)
        {
            createTask(i);

            if(i < 2)
            {
                yield return new WaitForSeconds(0.2f);
            }
        }
    }
}