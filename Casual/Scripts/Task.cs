﻿using Vivat;
using UnityEngine;

[System.Serializable]
public class Task
{
    public int id;
    public bool allowInverse;
    public Direction[] turns;
    public GameObject view;
}