﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vivat;

public class MatchController:MonoBehaviour, NotifyListener
{
    public int calcPerFrame = 3;
    public TaskController taskController;
    public InputController inputController;
    public BoardController boardController;

    List<Task> sortedTasks;

    [SerializeField]
    List<Direction> tempDirections;
    List<Turn> tempTurns;

    void Start()
    {
        sortedTasks = new List<Task>();
        tempDirections = new List<Direction>();
        tempTurns = new List<Turn>();

        NotificationCenter.addListener("endTurn", this);
    }

    public void onNotify(string eventId, params object[] args)
    {
        if(eventId == "endTurn")
        {
            onEndTurn(); 
        }
    }

    void onEndTurn()
    {
        tempTurns.Clear();
        tempTurns.AddRange(inputController.turns);
        
        filterTasks();

        if(sortedTasks.Count == 0)
        {
            return;
        }

        sortedTasks.Sort(compareByTurns);

        StopCoroutine("processMatchTask");
        StartCoroutine("processMatchTask");
    }

    void filterTasks()
    {
        sortedTasks.Clear();

        int countTurns = tempTurns.Count;
        int countTasks = taskController.maxTaskCount;
        for(int i = 0; i < countTasks; i++)
        {
            if(taskController.taskList[i] != null && 
                countTurns == taskController.taskList[i].turns.Length)
            {
                sortedTasks.Add(taskController.taskList[i]);
            }
        }
    }

    int compareByTurns(Task a, Task b)
    {
        return b.turns.Length - a.turns.Length;
    }

    IEnumerator processMatchTask()
    {
        int count = sortedTasks.Count;
        for(int i = 0; i < count; i++)
        {
            yield return null;

            if(checkMatchToTask(sortedTasks[i]))
            {
                completeTask(sortedTasks[i].id);
                yield break;
            }
        }
    }

    int getMatchedTaskId()
    {
        int count = sortedTasks.Count;
        for(int i = 0; i < count; i++)
        {
            if(checkMatchToTask(sortedTasks[i]))
            {
                return sortedTasks[i].id;
            }
        }
        return -1;
    }

    bool checkMatchToTask(Task task)
    {
        fillTemp();

        int count = tempDirections.Count;
        if(checkMatchToTaskWithRotations(task, count))
        {
            return true;
        }

        invertDirectionsBackward(count);
        if(checkMatchToTaskWithRotations(task, count))
        {
            return true;
        }

        if(task.allowInverse)
        {
            invertDirectionsHorizontal(count);
            if(checkMatchToTaskWithRotations(task, count))
            {
                return true;
            }

            invertDirectionsHorizontalAndBackward(count);
            if(checkMatchToTaskWithRotations(task, count))
            {
                return true;
            }
        }
                
        return false;
    }

    bool checkMatchToTaskWithRotations(Task task, int turnsCount)
    {
        for(int i = 0; i < 4; i++)
        {
            if(checkMatchToTaskRotate(task, turnsCount))
            {
                return true;
            }
        }
        return false;
    }

    void fillTemp()
    {
        tempDirections.Clear();

        int count = tempTurns.Count;
        for(int i = 0; i < count; i++)
        {
            tempDirections.Add(tempTurns[i].direction);
        }
    }

    bool checkMatchToTaskRotate(Task task, int count)
    {
        bool founded = true;
        for(int i = 0; i < count; i++)
        {
            if(tempDirections[i] != task.turns[i])
            {
                founded = false;
                break;
            }            
        }

        if(!founded)
        {
            rotateTempDirections(count);
            return false;
        }

        return true;
    }

    void rotateTempDirections(int count)
    {
        for(int i = 0; i < count; i++)
        {
            tempDirections[i] = DirectionTools.rotateDirection(tempDirections[i]);
        }
    }

    void invertDirectionsBackward(int count)
    {
        tempDirections.Clear();
        tempDirections.Add(tempTurns[0].direction);
        
        Direction dir;
        for(int i = count-1; i > 0; i--)
        {
            dir = DirectionTools.invertDirection(tempTurns[i].direction);
            tempDirections.Add(dir);
        }
    }

    void invertDirectionsHorizontal(int count)
    {
        tempDirections.Clear();

        Direction dir;
        for(int i = 0; i < count; i++)
        {
            dir = DirectionTools.invertDirectionHorizontal(tempTurns[i].direction);
            tempDirections.Add(dir);
        }
    }

    void invertDirectionsHorizontalAndBackward(int count)
    {
        tempDirections.Clear();
        tempDirections.Add(tempTurns[0].direction);

        Direction dir;
        for(int i = count - 1; i > 0; i--)
        {
            dir = DirectionTools.invertDirectionHorizontal(tempTurns[i].direction);
            dir = DirectionTools.invertDirection(dir);
            
            tempDirections.Add(dir);
        }
    }

    void invertDirectionsVertical(int count)
    {
        tempDirections.Clear();
        tempDirections.Add(tempTurns[0].direction);

        Direction dir;
        for(int i = count - 1; i > 0; i--)
        {
            dir = DirectionTools.invertDirectionVertical(tempTurns[i].direction);
            tempDirections.Add(dir);
        }
    }

    void completeTask(int id)
    {
        taskController.completeTask(id);
        
        int count = tempTurns.Count;
        for(int i = 0; i < count; i++)
        {
            boardController.clearCellOnPosition(tempTurns[i].x, tempTurns[i].y);
        }
        boardController.checkDepth();

        tempTurns.Clear();
    }
}