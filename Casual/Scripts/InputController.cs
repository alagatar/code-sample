﻿using UnityEngine;
using System.Collections.Generic;
using Vivat;

[System.Serializable]
public struct Turn
{
    public int x;
    public int y;
    public int depth;
    public Direction direction;
};

public class InputController:MonoBehaviour, NotifyListener
{
    public BoardController boardController;
    public List<Turn> turns;
    public bool tutorialMode = false;

    bool tapInited;
    Vector2 startPosition;
    Vector2 currentPosition;
    const float minDistance = 0.05f;
    List<Turn> turnsCache;
    bool active = true;
    Camera cam;
    List<IntPoint> tutorialCells;

    void Start()
    {
        turns = new List<Turn>();
        turnsCache = new List<Turn>();
        cam = Camera.main;
        NotificationCenter.addListener("changeLevelState", this);
    }

    void Update()
    {
        if(!active)
        {
            return;
        }

        if(Input.GetMouseButton(0))
        {
            if(!tapInited)
            {
                tapStart();
            }
            else
            {
                tapUpdate();
            }
        }
        else if(tapInited)
        {
            tapEnd();
        }
    }

    void tapStart()
    {
        startPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        var cell = boardController.getGridPosition(startPosition.x, startPosition.y);

        if(tutorialMode)
        {
            if(!checkCellInTutorialRange(cell))
            {
                return;
            }
        }

        if(!boardController.checkCellInGrid(cell.x, cell.y))
        {
            return;
        }
        
        tapInited = true;
        updateTurn();
    }

    void updateTurn()
    {
        var cell = boardController.getGridPosition(startPosition.x, startPosition.y);

        if(tutorialMode)
        {
            if(!checkCellInTutorialRange(cell))
            {
                return;
            }
        }

        if(!boardController.checkCellInGrid(cell.x, cell.y))
        {
            return;
        }

        if(checkPossibleTurnTo(cell))
        {
            if(checkTurnIsNew(cell))
            {
                saveTurn(cell);
            }
            else
            {
                clearTurn(cell);
            }
        }
    }

    bool checkPossibleTurnTo(IntPoint cell)
    {
        Piece piece = boardController.getPieceOnPosition(cell.x, cell.y);
        if(piece == null)
        {
            return false;
        }

        if(turns.Count == 0)
        {
            return true;
        }

        var lastTurn = turns[turns.Count - 1];

        //check change
        if(lastTurn.x == cell.x && lastTurn.y == cell.y)
        {
            return false;
        }

        //check dist
        float dist = Math.calcDistOfDots(lastTurn.x, lastTurn.y, cell.x, cell.y);
        if(dist > 1)
        {
            return false;
        }

        if(checkTurnIsNew(cell) && checkCellInTurns(cell.x, cell.y))
        {
            return false;
        }

        //check current turn depth 
        if(!checkEqualDepth(lastTurn.x, lastTurn.y, cell.x, cell.y))
        {
            return false;
        }

        return true;
    }

    bool checkEqualDepth(int x1, int y1, int x2, int y2)
    {
        var piece1 = boardController.getPieceOnPosition(x1, y1);
        var piece2 = boardController.getPieceOnPosition(x2, y2);

        return (piece1.depth == piece2.depth);
    }

    public bool checkCellInTurns(int col, int row)
    {
        int count = turns.Count;
        for(int i = 0; i < count; i++)
        {
            if(turns[i].x == col && 
                turns[i].y == row)
            {
                return true;
            }
        }
        return false;
    }

    bool checkTurnIsNew(IntPoint cell)
    {
        if(turns.Count < 2)
        {
            return true;
        }

        var prevTurn = turns[turns.Count - 2];
        if(prevTurn.x == cell.x && 
            prevTurn.y == cell.y)
        {
            return false;
        }

        return true;
    }

    void saveTurn(IntPoint cell)
    {
        Piece piece = boardController.getPieceOnPosition(cell.x, cell.y);

        var turn = getTurn();
        turn.x = cell.x;
        turn.y = cell.y;
        turn.depth = piece.depth;

        if(turns.Count == 0)
        {
            turn.direction = Direction.NONE;
        }
        else
        {
            var lastTurn = turns[turns.Count - 1];
            turn.direction = getDirection(lastTurn.x, lastTurn.y, cell.x, cell.y);
        }
        
        turns.Add(turn);

        NotificationCenter.sendNotify("addTurn");

        playEffect();
    }

    void playEffect()
    {
        if(turns.Count == 0)
        {
            return;
        }

        int count = Mathf.Clamp(turns.Count, 1, 8);
        string effectId = string.Format("c{0}", count);
        SoundManager.instance.playSfx(effectId);
    }

    Turn getTurn()
    {
        Turn newTurn;
        if(turnsCache.Count > 0)
        {
            int count = turnsCache.Count;
            newTurn = turnsCache[count - 1];
            turnsCache.RemoveAt(count - 1);
        }
        else
        {
            newTurn = new Turn();
        }
        return newTurn;
    }

    Direction getDirection(int x1, int y1, int x2, int y2)
    {
        int difX = x1 - x2;
        int difY = y1 - y2;

        if(difX < 0)
        {
            return Direction.RIGHT;
        }
        if(difX > 0)
        {
            return Direction.LEFT;
        }
        if(difY < 0)
        {
            return Direction.UP;
        }
        if(difY > 0)
        {
            return Direction.DOWN;
        }

        return Direction.NONE;
    }

    void clearTurn(IntPoint cell)
    {
        int count = turns.Count;
        turns.RemoveAt(count - 1);
        
        NotificationCenter.sendNotify("delTurn");

        playEffect();
    }

    void tapUpdate()
    {
        currentPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        float dist = Vector2.Distance(startPosition, currentPosition);
        if(dist < minDistance)
        {
            return;
        }
        startPosition = currentPosition;

        updateTurn();
    }

    void tapEnd()
    {
        tapInited = false;
        NotificationCenter.sendNotify("endTurn");

        cleanTurns();
    }

    void cleanTurns()
    {
        int count = turns.Count;
        if(count == 0)
        {
            return;
        }

        for(int i = 0; i < count; i++)
        {
            turnsCache.Add(turns[i]);
        }
        turns.Clear();
    }

    public void onNotify(string eventId, params object[] args)
    {
        string state = (string)args[0];

        if(state == "start" || state == "resume" || state == "reset")
        {
            startWork();
        }
        else if(state == "stop" || state == "pause")
        {
            stopWork();
        }
    }

    void startWork()
    {
        active = true;
    }

    void stopWork()
    {
        active = false;
        tapInited = false;
        cleanTurns();
    }

    public void startTutorial()
    {
        tutorialMode = true;
        tutorialCells = new List<IntPoint>();
    }

    public void setTutorialCells(List<IntPoint> cells)
    {
        tutorialCells.Clear();
        tutorialCells.AddRange(cells);
    }

    bool checkCellInTutorialRange(IntPoint cell)
    {
        int count = tutorialCells.Count;
        for(int i = 0; i < count; i++)
        {
            if(cell.x == tutorialCells[i].x && 
                cell.y == tutorialCells[i].y)
            {
                return true;
            }
        }
        return false;
    }

    public void completeTutorial()
    {
        tutorialMode = false;
        tutorialCells.Clear();
        tutorialCells = null;
    }
}