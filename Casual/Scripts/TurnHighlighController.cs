﻿using System.Collections.Generic;
using UnityEngine;
using Vivat;

public class TurnHighlighController:MonoBehaviour, NotifyListener
{
    public GameObject board;
    public InputController inputController;
    public GameObject pointPrefab;
    public GameObject jointPrefab;

    BoardController boardController;
    List<GameObject> points;
    List<GameObject> pointsCache;
    List<GameObject> joints;
    List<GameObject> jointsCache;
    Vector3 jointVerticalAngle;
    Vector3 jointHorizontalAngle;

    void Start()
    {
        if(board != null)
        {
            boardController = board.GetComponent<BoardController>();
        }

        NotificationCenter.addListener("addTurn", this);
        NotificationCenter.addListener("delTurn", this);
        NotificationCenter.addListener("endTurn", this);

        points = new List<GameObject>();
        pointsCache = new List<GameObject>();

        joints = new List<GameObject>();
        jointsCache = new List<GameObject>();
        jointVerticalAngle = new Vector3(0, 0, 90);
        jointHorizontalAngle = new Vector3();
    }

    public void onNotify(string eventId, params object[] args)
    {
        switch(eventId)
        {
            case "addTurn":
                onAddTurn();
                break;

            case "delTurn":
                onDelTurn();
                break;

            case "endTurn":
                onEndTurn();
                break;
        }
    }

    void onAddTurn()
    {
        int count = inputController.turns.Count;
        Turn lastTurn = inputController.turns[count - 1];
        
        createPoint(lastTurn);

        if(count > 1)
        {
            createJoint(lastTurn, count);
        }
    }

    void createPoint(Turn lastTurn)
    {
        GameObject point = getPoint();
        points.Add(point);
        
        FloatPoint position = boardController.getWorldPosition(lastTurn.x, lastTurn.y);
        point.transform.position = new Vector3(position.x, position.y, 0);
    }

    void createJoint(Turn lastTurn, int countTurns)
    {
        GameObject joint = getJoint();
        joints.Add(joint);

        if(lastTurn.direction == Direction.UP ||
            lastTurn.direction == Direction.DOWN)
        {
            joint.transform.eulerAngles = jointVerticalAngle;
        }
        else
        {
            joint.transform.eulerAngles = jointHorizontalAngle;
        }

        Turn prevTurn = inputController.turns[countTurns - 2];

        FloatPoint currentPosition = boardController.getWorldPosition(lastTurn.x, lastTurn.y);
        FloatPoint prevPosition = boardController.getWorldPosition(prevTurn.x, prevTurn.y);
        FloatPoint middlePosition = Math.getMiddlePoint(currentPosition.x, currentPosition.y, prevPosition.x, prevPosition.y);
        joint.transform.position = new Vector3(middlePosition.x, middlePosition.y, 0);
    }

    GameObject getPoint()
    {
        GameObject newItem;
        if(pointsCache.Count > 0)
        {
            int count = pointsCache.Count;
            newItem = pointsCache[count - 1];
            pointsCache.RemoveAt(count - 1);
        }
        else
        {
            newItem = Instantiate(pointPrefab, new Vector3(), Quaternion.identity) as GameObject;
        }
        newItem.transform.parent = transform;
        newItem.SetActive(true);
        return newItem;
    }

    GameObject getJoint()
    {
        GameObject newItem;
        if(jointsCache.Count > 0)
        {
            int count = jointsCache.Count;
            newItem = jointsCache[count - 1];
            jointsCache.RemoveAt(count - 1);
        }
        else
        {
            newItem = Instantiate(jointPrefab, new Vector3(), Quaternion.identity) as GameObject;
        }
        newItem.transform.parent = transform;
        newItem.SetActive(true);
        return newItem;
    }

    void onDelTurn()
    {
        int count = points.Count;
        delPoint(count - 1);

        if(count > 1)
        {
            delJoint(joints.Count - 1);
        }
    }

    void delPoint(int index, bool clear = true)
    {
        GameObject point = points[index];
        if(clear)
        {
            points.RemoveAt(index);
        }
        point.SetActive(false);
        pointsCache.Add(point);
    }

    void delJoint(int index, bool clear = true)
    {
        GameObject joint = joints[index];
        if(clear)
        {
            joints.RemoveAt(index);
        }
        joint.SetActive(false);
        jointsCache.Add(joint);
    }

    void onEndTurn()
    {
        int count = points.Count;
        if(count == 0)
        {
            return;
        }

        for(int i = 0; i < count; i++)
        {
            delPoint(i, false);
        }
        points.Clear();

        int jointsCount = joints.Count;
        for(int i = 0; i < jointsCount; i++)
        {
            delJoint(i, false);
        }
        joints.Clear();
    }
}