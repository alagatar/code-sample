﻿using UnityEngine;

[CreateAssetMenu(fileName = "BoardConfigData", menuName = "Custom/BoardConfig")]
public class BoardConfig:ScriptableObject
{
    public BoardRanksConfig[] ranks;

    public bool checkPresentConfig(int rank)
    {
        if(getConfigForRank(rank) != null)
        {
            return true;
        }
        return false;
    }

    public BoardRanksConfig getConfigForRank(int rank)
    {
        int count = ranks.Length;
        for(int i = 0; i < count; i++)
        {
            if(ranks[i].id == rank)
            {
                return ranks[i];
            }
        }
        return null;
    }
}

[System.Serializable]
public class BoardRanksConfig
{
    public int id;
    public BoardBlock[] blocks;
}

[System.Serializable]
public class BoardBlock
{
    public int x;
    public int y;
    public bool blocked;
}