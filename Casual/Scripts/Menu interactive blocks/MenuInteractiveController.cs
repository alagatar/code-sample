﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MenuInteractiveBlocks
{
    public class MenuInteractiveController:MonoBehaviour
    {
        public BaseMenuInteractiveBlock[] items;
        public float emptyPercent = 0.1f;
        public float repeatPercent = 0.1f;
        
        List<BaseMenuInteractiveBlock> avaiableItems = new List<BaseMenuInteractiveBlock>();

        void Start()
        {
            checkAvaiableItems();

            if(avaiableItems.Count > 0)
            {
                switchItem();
            }
        }

        void checkAvaiableItems()
        {
            avaiableItems.Clear();

            int count = items.Length;
            for(int i = 0; i < count; i++)
            {
                if(items[i].isAvaiable())
                {
                    avaiableItems.Add(items[i]);
                }
            }
        }

        void switchItem()
        {
            int skinIndex = getSkinIndex();
            if(skinIndex != -1)
            {
                showItem(skinIndex);
                return;
            }

            float percent = UnityEngine.Random.value;
            if(percent > emptyPercent)
            {
                showRandomItem();
            }
        }

        void showRandomItem()
        {
            int index = getRandomIndex();
            showItem(index);
        }

        int getRandomIndex()
        {
            if(avaiableItems.Count == 1)
            {
                return 0;
            }

            int index = UnityEngine.Random.Range(0, avaiableItems.Count);
            float percent = UnityEngine.Random.value;
            if(percent > repeatPercent)
            {
                index = indexWithouLast(index);
            }
            return index;
        }

        int getSkinIndex()
        {
            int count = avaiableItems.Count;
            for(int i = 0; i < count; i++)
            {
                if(avaiableItems[i] is Skin && (avaiableItems[i] as Skin).ready)
                {
                    return i;
                }
            }
            return -1;
        }

        int indexWithouLast(int index)
        {
            GameObject item = avaiableItems[index].gameObject;
            int itemIndex = Array.IndexOf(items, item);
            int lastItemIndex = PlayerPrefs.GetInt("lastInteractiveBlock", -1);
            if(itemIndex == lastItemIndex)
            {
                index++;
                if(index > avaiableItems.Count - 1)
                {
                    index = 0;
                }
            }
            return index;
        }

        void showItem(int index)
        {
            GameObject item = avaiableItems[index].gameObject;
            item.SetActive(true);

            int itemIndex = Array.IndexOf(items, item);
            PlayerPrefs.SetInt("lastInteractiveBlock", itemIndex);
        }
    }
}