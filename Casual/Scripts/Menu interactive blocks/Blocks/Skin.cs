﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vivat;

namespace MenuInteractiveBlocks
{
    public class Skin:BaseMenuInteractiveBlock
    {
        public int price;
        public Transform pricePanel;
        public Text priceField;
        public Button button;
        public GameObject progress;
        public Image progressImage;
        public bool ready = false;

        List<int> avaiableSkins = new List<int>();
        
        void Start()
        {
            if(Main.instance.coins >= price)
            {
                activeState();
            }
            else
            {
                normalState();
            }
        }

        void getAvaiableSkins()
        {
            for(int i = 1; i < 20; i++)
            {
                if(PlayerPrefs.GetInt(string.Format("blockSkin{0}", i), 0) == 0)
                {
                    avaiableSkins.Add(i);
                }
            }
        }

        void activeState()
        {
            button.gameObject.SetActive(true);
            progress.SetActive(false);
            Vector2 pos = pricePanel.localPosition;
            pos.x = -26f;
            pricePanel.localPosition = pos;

            priceField.text = "" + price;
        }

        void normalState()
        {
            button.gameObject.SetActive(false);
            progress.SetActive(true);
            Vector2 pos = pricePanel.localPosition;
            pos.x = -50f;
            pricePanel.localPosition = pos;
            
            int coins = Main.instance.coins;
            priceField.text = string.Format("{0}/{1}", coins, price);

            float percent = Tools.toPercent(coins, price);
            if(percent > 1f)
            {
                percent = 1f;
            }
            progressImage.fillAmount = percent;
        }

        override public void activate()
        {
            base.activate();

            Main.instance.changeCoins(-price);

            int index = UnityEngine.Random.Range(0, avaiableSkins.Count);
            int randomSkin = avaiableSkins[index];
            Main.instance.unlockSkin(randomSkin);
            PlayerPrefs.SetInt("blockSkin", randomSkin);
            Main.instance.skin = randomSkin;

            NotificationCenter.sendNotify("showMenu", "newSkin", randomSkin);
        }

        override public bool isAvaiable()
        {
            if(Main.instance.coins >= price)
            {
                ready = true;
            }
            else
            {
                ready = false;
            }

            getAvaiableSkins();
            if(avaiableSkins.Count > 0)
            {
                return true;
            }
            return false;
        }
    }
}