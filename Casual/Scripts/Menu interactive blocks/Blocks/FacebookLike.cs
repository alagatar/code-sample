﻿using UnityEngine;

namespace MenuInteractiveBlocks
{
    public class FacebookLike:BaseMenuInteractiveBlock
    {
        override public bool isAvaiable()
        {
            if(PlayerPrefs.GetInt("facebookLike", 0) == 0)
            {
                return true;
            }
            return false;
        }

        override public void activate()
        {
            base.activate();

            Main.instance.likeOnFacebook();
            Main.instance.unlockSkin(1);
        }
    }
}