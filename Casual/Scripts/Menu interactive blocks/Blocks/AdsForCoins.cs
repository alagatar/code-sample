using Vivat;

namespace MenuInteractiveBlocks
{
    public class AdsForCoins:BaseMenuInteractiveBlock, NotifyListener
    {
        public int coinsMin = 1;
        public int coinsMax = 10;

        override public bool isAvaiable()
        {
            if(TapdaqController.instance.checkRewardedReady())
            {
                return true;
            }
            return false;
        }

        override public void activate()
        {
            base.activate();

            TapdaqController.instance.showRewarded("menu-coins");
        }

        private void Start()
        {
            NotificationCenter.addListener("adEvent", this);
        }

        public void onNotify(string eventId, params object[] args)
        {
            if((string)args[0] == "end")
            {
                onAdComplete((string)args[1]);
            }
        }

        void onAdComplete(string adId)
        {
            if(adId == "menu-coins")
            {
                int randomCoins = Tools.randomIntDivisible(coinsMin, coinsMax, 5);
                NotificationCenter.sendNotify("showMenu", "reward", randomCoins);
            }
        }
    }
}