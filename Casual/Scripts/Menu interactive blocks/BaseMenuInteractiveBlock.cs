﻿using System.Collections;
using UnityEngine;

namespace MenuInteractiveBlocks
{
    public class BaseMenuInteractiveBlock:MonoBehaviour
    {
        public Animator buttonAnimator;

        static readonly int click_show = Animator.StringToHash("click");
        
        public virtual bool isAvaiable()
        {
            return true;
        }

        public virtual void click()
        {
            buttonAnimator.SetTrigger(click_show);
            StartCoroutine("waitAnim");
        }

        IEnumerator waitAnim()
        {
            yield return new WaitForSeconds(0.35f);
            activate();
        }

        public virtual void activate()
        {
            gameObject.SetActive(false);
        }
    }
}