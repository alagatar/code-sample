﻿using UnityEngine;

[CreateAssetMenu(fileName = "TasksConfigData", menuName = "Custom/TasksConfig")]
public class TasksConfig:ScriptableObject
{
    public Task[] items;
    public RankConfig[] ranks;

    public bool checkPresentConfig(int rank)
    {
        if(getConfigForRank(rank) != null)
        {
            return true;
        }
        return false;
    }

    public RankConfig getConfigForRank(int rank)
    {
        int count = ranks.Length;
        for(int i = 0; i < count; i++)
        {
            if(ranks[i].id == rank)
            {
                return ranks[i];
            }

        }
        return null;
    }
}

[System.Serializable]
public class RankConfig
{
    public int id;
    public int[] items;
}