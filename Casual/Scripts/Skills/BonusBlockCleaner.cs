﻿using UnityEngine;
using UnityEngine.UI;
using Vivat;

public class BonusBlockCleaner:MonoBehaviour, NotifyListener
{
    public Animator animator;
    public Image icon;
    public Image bar;
    public float currentProgress = 0;
    public float maxProgress = 10;
    public ClearEffectController effect;

    bool blinking = false;

    static readonly int anim_show = Animator.StringToHash("show");
    static readonly int anim_hide = Animator.StringToHash("hide");

    void Start()
    {
        bar.fillAmount = 0f;

        //NotificationCenter.addListener("increaseScore", this);
        NotificationCenter.addListener("changeLevelState", this);

        NotificationCenter.addListener("increaseSkillProgress", this);
    }

    public void onNotify(string eventId, params object[] args)
    {
        //if(eventId == "increaseScore")
        //{
        //    int value = (int)args[1];//0 with multiplier, 1 without multiplier
        //    increaseProgress(value);
        //}
        if(eventId == "increaseSkillProgress" &&
            (string)args[0] == "coins")
        {
            increaseProgress((int)args[1]);
        }
        else if(eventId == "changeLevelState")
        {
            onChangeLevelState((string)args[0]);
        }
    }

    void increaseProgress(int value)
    {
        currentProgress += value;
        if(currentProgress >= maxProgress)
        {
            int count = Mathf.FloorToInt(currentProgress / maxProgress);
            currentProgress -= (maxProgress * count);
            complete(count);
        }
        updateBar();
    }

    void complete(int count)
    {
        showEffect();
    }

    void showEffect()
    {
        Job newJob = new Job();
        newJob.callback = effect.show;
        
        NotificationCenter.sendNotify("jobEvent", "new", newJob);
    }

    void updateBar()
    {
        float percent = currentProgress / maxProgress;

        if(percent >= 0.8f && !blinking)
        {
            blinking = true;
            animator.SetTrigger(anim_show);
        }

        if(percent < 0.8f && blinking)
        {
            blinking = false;
            animator.SetTrigger(anim_hide);
        }

        bar.fillAmount = percent;
    }

    void onChangeLevelState(string state)
    {
        if(state == "reset")
        {
            onResetGame();
        }
    }

    void onResetGame()
    {
        currentProgress = 0;
        updateBar();
    }
}