﻿using UnityEngine;
using DG.Tweening;
using Vivat;
using System.Collections;
using UnityEngine.UI;

public class ClearEffectController:MonoBehaviour
{
    public Animator animator;
    public GameObject icon;
    public Image iconBody;
    public Transform startPosition;
    public Vector2 jumpTarget;
    public HidePanel hidePanel;

    bool inited = false;
    Vector2 targetPosition;

    static readonly int anim_show = Animator.StringToHash("show");

    public void show()
    {
        NotificationCenter.sendNotify("changeLevelState", "pause");

        if(!inited)
        {
            inited = true;
            icon.transform.position = setPositionFrom(icon.transform.position, startPosition.position);
        }

        hidePanel.show();
        
        icon.SetActive(true);

        Sequence tween = DOTween.Sequence();
        tween.Append(icon.transform.DOLocalJump(jumpTarget, 300f, 1, 0.5f).SetEase(Ease.Flash));
        tween.Append(icon.transform.DOScale(1.7f, 0.2f));
        tween.Append(icon.transform.DOScale(1f, 0.15f));
        tween.OnComplete(completeShow);

        SoundManager.instance.playSfx("jump");
    }

    void completeShow()
    {
        animator.enabled = true;
        animator.SetTrigger(anim_show);
        StartCoroutine("waitAndComplete");

        SoundManager.instance.playSfx("skill-clear");
    }

    IEnumerator waitAndComplete()
    {
        yield return new WaitForSeconds(0.4f);
        showEffect();
        yield return new WaitForSeconds(0.4f);
        complete();
    }

    void showEffect()
    {
        NotificationCenter.sendNotify("resetGridHead");
    }

    void complete()
    {
        icon.transform.position = setPositionFrom(icon.transform.position, startPosition.position);
        icon.SetActive(false);
        animator.enabled = false;

        Color targetColor = iconBody.color;
        targetColor.a = 1f;
        iconBody.color = targetColor;

        Vector2 targetScale = iconBody.transform.localScale;
        targetScale.x = 0.5f;
        targetScale.y = 0.5f;
        iconBody.transform.localScale = targetScale;
        
        hidePanel.hide();

        NotificationCenter.sendNotify("changeLevelState", "resume");

        NotificationCenter.sendNotify("jobEvent", "complete");
    }

    Vector2 setPositionFrom(Vector2 target, Vector2 source)
    {
        Vector2 pos = target;
        pos.x = source.x;
        pos.y = source.y;
        return pos;
    }
}