using UnityEngine;
using Vivat;

public class Main:MonoBehaviour, NotifyListener
{
    public static Main instance;
    public int coins;
    public int skin;
    public int rank;
    public string facebookPage = "https://www.facebook.com/homiebearstudio";
    public string androidAppId;
    public string iosAppId;

    bool inited;

    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

            initSettings();
        }
    }

    void initSettings()
    {
        Application.targetFrameRate = 30;
        QualitySettings.vSyncCount = 0;
        QualitySettings.antiAliasing = 0;

        //PlayerPrefs.SetInt("rankValue", 1);
        //PlayerPrefs.SetInt("coins", 99999);

        //PlayerPrefs.DeleteAll();
        //PlayerPrefs.Save();

        coins = PlayerPrefs.GetInt("coins");
        skin = PlayerPrefs.GetInt("blockSkin", 0);
        rank = PlayerPrefs.GetInt("rankValue", 1);
    }

    void Start()
    {
        if(!inited)
        {
            init();
        }
    }

    void init()
    {
        inited = true;

        NotificationCenter.addListener("buyRemoveAds", this, "global");
        Social.localUser.Authenticate(processAuthentication);
    }

    void processAuthentication(bool success)
    {
        if(success)
        {
            //Debug.Log("Authenticated");
        }
        else
        {
            //Debug.Log("Failed to authenticate");
        } 
    }

    public void changeCoins(int value)
    {
        coins += value;

        PlayerPrefs.SetInt("coins", coins);

        NotificationCenter.sendNotify("changeCoins");
    }

    public void onNotify(string eventId, params object[] args)
    {
        showRemoveAds();
    }

    void showRemoveAds()
    {
        //completeRemoveAds();
    }

    void completeRemoveAds()
    {
        TapdaqController.instance.disableAds();

        NotificationCenter.sendNotify("removeAds");
    }

    public void changeRank(int value)
    {
        rank = value;
        PlayerPrefs.SetInt("rankValue", rank);
    }

    public void sendScore(int score)
    {
        if(!Social.localUser.authenticated)
        {
            //Debug.Log("No auth");
            return;
        }
        Social.ReportScore(score, "leaderBoardId", highScoreCallback);
    }

    void highScoreCallback(bool sucess)
    {
        if(sucess)
        {
            //Debug.Log("Score submission successful");
        }   
        else
        {
            //Debug.Log("Score submission failed");
        }  
    }

    //0.0 - 100.0
    public void sendAchievement(string id, float progress)
    {
        print(string.Format("{0}: {1}%", id, progress));

        if(!Social.localUser.authenticated)
        {
            //Debug.Log("No auth");
            return;
        }
        Social.ReportProgress(id, progress, achievementsCallback);
    }

    void achievementsCallback(bool sucess)
    {
        if(sucess)
        {
            //Debug.Log("Achievement submission successful");
        }
        else
        {
            //Debug.Log("Achievement submission failed");
        }
    }

    public void likeOnFacebook()
    {
        PlayerPrefs.SetInt("facebookLike", 1);

        Application.OpenURL(facebookPage);
    }

    public void rateIt()
    {
        #if UNITY_ANDROID
                Application.OpenURL("market://details?id=" + androidAppId);
        //Application.OpenURL ("market://details?id=com.example.android"); 
        #elif UNITY_IOS
                        Application.OpenURL("itms-apps://itunes.apple.com/app/" + iosAppId);
                        //"itms-apps://itunes.apple.com/app/id1160648791";
        #endif

        PlayerPrefs.SetInt("rated", 1);
    }

    public void unlockSkin(int id)
    {
        PlayerPrefs.SetInt("blockSkin" + id, 1);
        AchievementManager.instance.trigger("st_level");
    }
}