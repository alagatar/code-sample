using System;
using UnityEngine;
using Vivat;

public class Character:MonoBehaviour
{
    public Rigidbody body;
    public Transform graphContainer;
    public float speed = 1f;
    public float health = 100f;
    public float maxHealth = 100f;
    public Direction side = Direction.Right;
    [HideInInspector]
    public int maxTargets = 1;
    [HideInInspector]
    public bool active = true;
    public Action onHit;
    [HideInInspector]
    public Collider[] checkHitResult;
    public LayerMask enemyMask;

    private Quaternion hitRotation = Quaternion.identity;

    private void OnValidate()
    {
        if(health < 0)
        {
            health = 0;
        }

        if(maxHealth < 0)
        {
            maxHealth = 0;
        }

        if(health > maxHealth)
        {
            maxHealth = health;
        }

        if(maxHealth < health)
        {
            health = maxHealth;
        }
    }

    private void Start()
    {        
        checkHitResult = new Collider[maxTargets];
    }

    public void hit(float value)
    {
        if(!active)
        {
            return;
        }        

        //sent event to triger with value

        health -= value;
        health = Mathf.Clamp(health, 0f, maxHealth);

        if(onHit != null)
        {
            onHit();
        }

        if(Mathf.Approximately(health, 0f))
        {
            kill();
        }
    }

    public void move(Vector3 velocity)
    {
        body.velocity = Vector3.zero;
        body.AddForce(velocity, ForceMode.Force);
    }

    public int checkArea(Vector3 center, Vector3 halfAreaSize)
    {
        return Physics.OverlapBoxNonAlloc(center, halfAreaSize, checkHitResult, hitRotation, enemyMask);
    }

    public int checkAreaCircle(Vector3 center, float radius)
    {
        return Physics.OverlapSphereNonAlloc(center, radius, checkHitResult, enemyMask);
    }

    private void kill()
    {
        active = false;
        gameObject.SetActive(false);
    }

    public Vector3 getAttackAreaPos(Vector3 attackPositionOffset, Vector3 attackAreaSize)
    {
        Vector3 targetPos = transform.position;
        targetPos.y += attackPositionOffset.y;
        targetPos.z += attackPositionOffset.z;

        float xDif = attackPositionOffset.x + (attackAreaSize.x * 0.5f);
        if(side == Direction.Right)
        {
            targetPos.x += xDif;
        }
        else
        {
            targetPos.x -= xDif;
        }
        return targetPos;
    }

    public T getFirstTargetInRadius<T>(float radius) where T : MonoBehaviour
    {
        int count = checkAreaCircle(transform.position, radius);
        if(count == 0)
        {
            return null;
        }

        GameObject enemyObject;
        T target;
        for(int i = 0; i < count; i++)
        {
            enemyObject = checkHitResult[i].gameObject.transform.parent.gameObject;
            target = enemyObject.GetComponent<T>();
            if(target != null)
            {
                return target;
            }
        }

        return null;
    }

    public void changeSide(Direction newSide)
    {
        side = newSide;

        var scale = graphContainer.localScale;
        if(newSide == Direction.Right)
        {
            scale.x = -1;
        }
        else
        {
            scale.x = 1;
        }
        graphContainer.localScale = scale;
    }
}
