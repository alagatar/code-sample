using System.Collections;
using UnityEngine;
using Vivat;

public class PlayerController:MonoBehaviour
{
    public int id = 1;
    public Character character;
    public float strafeTime = 1f;
    public float strafePower = 1f;
    [Range(0, 1)]
    public float verticalSpeedCoef = 1f;
    public Vector3 attackAreaSize = new Vector3(0.5f, 0.5f, 0.25f);
    public Vector3 attackPositionOffset = new Vector3();
    public PlayerUiController uiController;
    public bool drawAttackAreaGizmo;
    public Animator animController;

    private Vector3 velocity = Vector3.zero;
    private Vector3 prevVelocity = Vector3.zero;
    private Vector3 zeroVelocity = Vector3.zero;
    private bool canMove;
    private bool strafing;
    private WaitForSeconds strafeStartTimeout = new WaitForSeconds(0.1f);
    private bool attacking = false;
    private WaitForSeconds attackCompleteTimeout = new WaitForSeconds(0.3f);
    private int attackAnimId = 1;
    private WaitForSeconds attackComboTimeout = new WaitForSeconds(0.5f);
    private bool hitting = false;
    private WaitForSeconds hitCompleteTimeout = new WaitForSeconds(0.3f);

    private void Start()
    {
        character.onHit = onHit;
        uiController.initFrom(this);
    }

    private void onHit()
    {
        uiController.updateHealth(character.health);
        animController.SetTrigger(AnimatorHash.TAKE_DAMAGE);

        hitting = true;
        StopCoroutine(waitCompleteHit());
        StartCoroutine(waitCompleteHit());
    }

    private IEnumerator waitCompleteHit()
    {
        yield return hitCompleteTimeout;
        hitting = false;
    }

    private void Update()
    {
        if(!strafing)
        {
            updateInput();
        }
    }

    private void updateInput()
    {
        calcVelocity();

        if(checkCanMove())
        {
            canMove = true;
            prevVelocity = velocity;

            animController.SetFloat(AnimatorHash.VELOCITY, velocity.magnitude);
        }
        else if(canMove)
        {
            canMove = false;
            velocity = zeroVelocity;
            character.body.velocity = zeroVelocity;
            animController.SetFloat(AnimatorHash.VELOCITY, 0f);
        }

        if(hitting)
        {
            return;
        }

        if(Input.GetButtonDown("A button"))
        {
            startStrafe();
        }

        if(Input.GetButtonDown("B button"))
        {
            if(checkInteractTarget())
            {
                interact();
            }
            else
            {
                print("No interact target");
            }
        }

        if(Input.GetButtonDown("X button"))
        {
            attack();
        }
        else
        {
            animController.SetInteger(AnimatorHash.ATTACK, 0);
        }

        if(Input.GetButtonDown("Y button"))
        {
            skill();
        }
    }

    private void calcVelocity()
    {
        velocity.x = Input.GetAxis("Horizontal");
        velocity.z = Input.GetAxis("Vertical");

        if(velocity.sqrMagnitude < 0.001f)
        {
            velocity.x = Input.GetAxis("D-pad horizontal");
            velocity.z = Input.GetAxis("D-pad vertical");
        }

        velocity.x *= character.speed;
        velocity.z *= character.speed * verticalSpeedCoef;
    }

    private bool checkCanMove()
    {
        if(hitting || attacking || velocity.sqrMagnitude < 0.1f)
        {
            return false;
        }
        return true;
    }

    private bool checkInteractTarget()
    {
        return false;
    }

    private void interact()
    {
        print("Interact");
    }

    private void attack()
    {
        print("Attack id: " + attackAnimId);
        animController.SetInteger(AnimatorHash.ATTACK, attackAnimId);
        attackAnimId++;
        if(attackAnimId > 3)
        {
            attackAnimId = 1;
        }

        int countEnemys = checkAttackArea();
        if(countEnemys > 0)
        {
            GameObject enemyObject;
            Character enemy;
            for(int i = 0; i < countEnemys; i++)
            {
                enemyObject = character.checkHitResult[i].gameObject.transform.parent.gameObject;
                enemy = enemyObject.GetComponent<Character>();

                if(enemy.active)
                {
                    //enemy.hit(weapon.damage);
                    enemy.hit(10f);
                }
                else
                {
                    character.checkHitResult[i] = null;
                }
            }
        }

        attacking = true;
        StopCoroutine(waitCompleteAttack());
        StartCoroutine(waitCompleteAttack());
        
        StopCoroutine(waitCompleteAttackCombo());
        StartCoroutine(waitCompleteAttackCombo());
    }

    private IEnumerator waitCompleteAttack()
    {
        yield return attackCompleteTimeout;
        attacking = false;
    }

    private IEnumerator waitCompleteAttackCombo()
    {
        yield return attackComboTimeout;
        attackAnimId = 1;
    }

    private int checkAttackArea()
    {
        Vector3 targetPos = character.getAttackAreaPos(attackPositionOffset, attackAreaSize);
        return character.checkArea(targetPos, attackAreaSize * 0.5f);
    }

    private void skill()
    {
        print("Skill");
    }

    private void FixedUpdate()
    {
        if(canMove)
        {
            move();
        }
    }

    private void move()
    {
        character.move(velocity);
        updateSide(velocity);
    }

    private void updateSide(Vector2 velocity)
    {
        if(velocity.x > 0 && character.side != Direction.Right)
        {
            character.changeSide(Direction.Right);
        }
        else if(velocity.x < 0 && character.side != Direction.Left)
        {
            character.changeSide(Direction.Left);
        }
    }

    private void startStrafe()
    {
        strafing = true;
        canMove = false;

        animController.SetTrigger(AnimatorHash.STRAFE);
        StartCoroutine(waitStartStrafe());

        StopCoroutine(waitCompleteAttack());
        attacking = false;
    }

    private IEnumerator waitStartStrafe()
    {
        yield return strafeStartTimeout;
        runStrafe();
    }

    private void runStrafe()
    {
        if(prevVelocity.magnitude < 0.001f)
        {
            prevVelocity.x = 1f;
            if(character.side == Direction.Left)
            {
                prevVelocity.x *= -1;
            }
        }
        Vector3 force = prevVelocity.normalized * strafePower;

        character.body.velocity = zeroVelocity;
        character.body.AddForce(force, ForceMode.Impulse);

        StartCoroutine(waitCompleteStrafe());
    }

    private IEnumerator waitCompleteStrafe()
    {
        yield return new WaitForSeconds(strafeTime);

        stopStrafe();
    }

    private void stopStrafe()
    {
        strafing = false;

        character.body.velocity = zeroVelocity;
        velocity = zeroVelocity;
    }

    private void OnCollisionEnter(Collision other)
    {
        if(strafing)
        {
            StopCoroutine("waitCompleteStrafe");
            stopStrafe();
        }
    }

    private void OnDrawGizmos()
    {
        Color gizmoColor = Gizmos.color;

        if(drawAttackAreaGizmo)
        {
            drawAttackArea();
        }

        Gizmos.color = gizmoColor;
    }

    private void drawAttackArea()
    {
        Gizmos.color = Color.red;

        Vector3 targetPos = character.getAttackAreaPos(attackPositionOffset, attackAreaSize);
        Gizmos.DrawWireCube(targetPos, attackAreaSize);
    }
}
