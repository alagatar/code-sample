using TMPro;
using UnityEngine;

public class PlayerUiController:MonoBehaviour
{
    public HealthBar healthBar;
    public TextMeshProUGUI nameField;

    public void initFrom(PlayerController player)
    {
        nameField.text = string.Format("Player <{0}>", player.id);
        healthBar.init(player.character.health, player.character.maxHealth);
    }

    public void updateHealth(float value)
    {
        healthBar.currentHealth = value;
    }

    public void updateMaxHealth(float value)
    {
        healthBar.maxHealth = value;
    }
}