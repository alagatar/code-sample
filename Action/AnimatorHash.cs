using UnityEngine;

public static class AnimatorHash
{
    public static readonly int VELOCITY = Animator.StringToHash("Velocity");
    public static readonly int STRAFE = Animator.StringToHash("Strafe");
    public static readonly int TAKE_DAMAGE = Animator.StringToHash("Take damage");
    public static readonly int ATTACK = Animator.StringToHash("Attack");
}