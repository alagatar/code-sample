using UnityEngine;

public class Area:MonoBehaviour
{
    public Vector3 size = Vector3.zero;
    public Vector3 offset = Vector3.zero;
    public Color color = Color.green;
    public Bounds bounds;

    private void OnDrawGizmos()
    {
        Color gizmosColor = Gizmos.color;

        Gizmos.color = color;
        //Vector3 pos = transform.position + offset;
        //Gizmos.DrawWireCube(pos, size);
        Gizmos.DrawWireCube(bounds.center, bounds.size);

        Gizmos.color = gizmosColor;
    }
}