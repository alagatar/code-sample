using System;
using UnityEngine;

public class CameraFollow:MonoBehaviour
{
    public Camera cam;
    public Transform target;
    public Area border;
    public float smoothing = 10;
    
    private Bounds camBounds;
    Vector3 wantedPosition;

    void Start()
    {
        camBounds = cam.orthographicBounds();
        Vector3 size = camBounds.size;
        size.y *= 1.42f;
        camBounds.size = size;
    }

    private void LateUpdate()
    {
        Vector3 targetPos = target.position;
        targetPos.z -= (camBounds.size.y / 2);

        wantedPosition = targetPos;
        wantedPosition.y = cam.transform.position.y;

        float minX = border.bounds.min.x + (camBounds.size.x / 2);
        float maxX = border.bounds.max.x - (camBounds.size.x / 2);
        float minY = border.bounds.min.y;
        float maxY = border.bounds.max.y - camBounds.size.y;
        
        wantedPosition.x = Mathf.Clamp(wantedPosition.x, minX, maxX);
        wantedPosition.z = Mathf.Clamp(wantedPosition.z, minY, maxY);

        transform.position = Vector3.Lerp(transform.position, wantedPosition, (Time.deltaTime * smoothing));
    }

    private void OnDrawGizmos()
    {
        Color gizmosColor = Gizmos.color;

        drawCameraBounds();

        Gizmos.color = gizmosColor;
    }

    private void drawCameraBounds()
    {
        Gizmos.color = Color.red;

        //Vector3 pos = transform.position;
        //Vector3 size = Vector3.zero;
        //size.y = cam.orthographicSize * 2f;
        //size.x = size.y * cam.aspect;



        //Gizmos.DrawWireCube(pos, size);

        //Gizmos.DrawWireCube(cam.rect.center, cam.rect.size);

        //Vector3 size = camBounds.size;
        //size.y *= 1.41f;

        Gizmos.DrawWireCube(wantedPosition, camBounds.size);

    }
}
